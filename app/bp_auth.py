import os

from flask import (
    Blueprint,
    g,
    redirect,
    request,
    session,
    url_for,
)

from flask_login import (
    login_required,
    login_user,
    logout_user,
)


from app.db import get_db
from app.auth.google_sso import (
    get_login_sso_request,
    set_google_sso_callback,
)
from app.user import User

bp = Blueprint("auth", __name__, url_prefix="/auth")


@bp.route("/google_sso", methods=["GET"])
def google_sso():
    return redirect(get_login_sso_request())


@bp.route("/google_sso/callback", methods=("GET", "POST"))
def callback():
    unique_id, user_name, user_email = set_google_sso_callback(request)
    # Check if user is in DB, create if non-existant
    if not User.get_by_google_id(unique_id):
        User.create(
            google_id=unique_id,
            user_name=user_name,
            user_email=user_email,
        )
    user = User.get_by_google_id(unique_id)
    login_user(user)
    return redirect(url_for("index"))


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get("_user_id")

    if user_id is None:
        g.user = None
    else:
        g.user = User.get_by_user_id(user_id)


@bp.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for("index"))


@bp.route("/local_login")
def local_login():
    if os.getenv("LOCAL_USER") == "true":
        user = User.get_by_google_id("999999")
        if not user:
            User.create(
                google_id="999999",
                user_name="localuser",
                user_email="localuser@local.com",
            )
            user = User.get_by_google_id("999999")
        login_user(user)
        return redirect(url_for("index"))
    return "Local user mode is not enabled.", 403
