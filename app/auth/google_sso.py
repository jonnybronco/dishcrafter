import requests
import os
import json
from oauthlib.oauth2 import WebApplicationClient
from flask import request


# Configuration
GOOGLE_CLIENT_ID = os.environ.get("GOOGLE_CLIENT_ID", None)
GOOGLE_CLIENT_SECRET = os.environ.get("GOOGLE_CLIENT_SECRET", None)
GOOGLE_DISCOVERY_URL = "https://accounts.google.com/.well-known/openid-configuration"

# OAuth 2 client setup
client = WebApplicationClient(GOOGLE_CLIENT_ID)


def get_login_sso_request():
    """Create a proper request URI for google soo

    Returns:
        request_uri: google_sso reuquest uri
    """
    google_provider_cfg = requests.get(GOOGLE_DISCOVERY_URL).json()
    authorization_endpoint = google_provider_cfg["authorization_endpoint"]

    request_uri = client.prepare_request_uri(
        authorization_endpoint,
        redirect_uri=request.base_url + "/callback",
        scope=["openid", "email", "profile"],
    )
    return request_uri


def _sso_login_process(request):
    # Get authorization code Google sent back to you
    code = request.args.get("code")
    # Find out what URL to hit to get tokens that allow you to ask for
    # things on behalf of a user
    google_provider_cfg = requests.get(GOOGLE_DISCOVERY_URL).json()
    token_endpoint = google_provider_cfg["token_endpoint"]
    # Prepare and send a request to get tokens!
    token_url, headers, body = client.prepare_token_request(
        token_endpoint,
        authorization_response=request.url,
        redirect_url=request.base_url,
        code=code,
    )
    token_response = requests.post(
        token_url,
        headers=headers,
        data=body,
        auth=(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET),
    )

    # Parse the tokens!
    client.parse_request_body_response(json.dumps(token_response.json()))
    # Now that you have tokens let's find and hit the URL
    # from Google that gives you the user's profile information,
    # including their Google profile image and email
    userinfo_endpoint = google_provider_cfg["userinfo_endpoint"]
    uri, headers, body = client.add_token(userinfo_endpoint)
    userinfo_response = requests.get(uri, headers=headers, data=body)
    # You want to make sure their email is verified.
    # The user authenticated with Google, authorized your
    # app, and now you've verified their email through Google!
    return userinfo_response


def set_google_sso_callback(request):
    """Once a sso request is getted, this function does all the setup,
    such that a proper user login can happen.

    Args:
        request (_flask_request): _the request variable passed from the flask app._

    Returns:
        _unique_id_: _Unique User ID by google_
        _user_name_: _The Google Username_
        _user_email_: _The Google Email Address_
    """
    userinfo_response = _sso_login_process(request=request)
    if userinfo_response.json().get("email_verified"):
        unique_id = (userinfo_response.json()["sub"],)
        user_name = (userinfo_response.json()["given_name"],)
        user_email = userinfo_response.json()["email"]
        return unique_id[0], user_name[0], user_email
    else:
        unique_id = None
        user_name = None
        user_email = None
        return unique_id, user_name, user_email
