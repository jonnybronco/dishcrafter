import random
from datetime import datetime

from flask import (
    Blueprint,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    flash,
    url_for,
)
from flask_login import login_required
from app.recipe import Recipe
from app.mealplan import MealPlan
from html import escape

bp = Blueprint("mealplan", __name__, url_prefix="/mealplan")

DAYS_OF_THE_WEEK = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
]


@bp.route("/saveCurrent", methods=["POST"])
def save_current():
    user_id = session.get("_user_id")
    data = request.json
    print(data)
    MealPlan.save_current_mealplan(user_id=user_id, mealplan_data=data)
    data = MealPlan.get_last_mealplan(user_id=user_id)
    if data:
        flash("Success, saved mealplan!", "success")
    else:
        flash("Something went wrong, could not save mealplan!", "error")
    return jsonify({"status": "success"})


@bp.route("/load_last_mealplan")
def load_last_mealplan():
    user_id = session.get("_user_id")
    last_mealplan_data = MealPlan.get_last_mealplan(user_id=user_id)

    if last_mealplan_data:
        # Extract the menuItems from the last mealplan
        menu_items = last_mealplan_data.get("menuItems", [])

        # Parse the meal plan data into the expected structure
        meal_plan = {}
        for item in menu_items:
            day = item["day"]
            meal_type = item["meal"]
            recipe = Recipe.get_recipe_by_id(item["recipeId"])

            if day not in meal_plan:
                meal_plan[day] = {"lunch": None, "dinner": None}

            meal_plan[day][meal_type] = recipe

        # Render the overview with the loaded meal plan
        return overview(meal_plan_saved=meal_plan)
    else:
        flash("No saved meal plan found.", "error")
        return redirect(url_for("mealplan.overview"))


@bp.route("/")
def overview(meal_plan_saved=None):
    user_id = session.get("_user_id")

    if meal_plan_saved is None:
        recipes = Recipe.get_recipes_for_user_id(user_id=user_id)
        shared_recipes = Recipe.get_shared_recipes(shared_with_user_id=user_id)
        recipes = recipes + shared_recipes
        # Shuffle recipes for meal planning
        random.shuffle(recipes)

        # Get the current day and reorder the DAYS_OF_THE_WEEK
        current_day = datetime.today().strftime("%A")
        start_index = DAYS_OF_THE_WEEK.index(current_day)
        ordered_days_of_the_week = (
            DAYS_OF_THE_WEEK[start_index:] + DAYS_OF_THE_WEEK[:start_index]
        )

        num_days = len(ordered_days_of_the_week)
        recipes_per_day = 2  # for lunch and dinner
        total_used = min(len(recipes), num_days * recipes_per_day)
        remaining_recipes = recipes[total_used:]
        meal_plan = {}
        for i, day in enumerate(ordered_days_of_the_week):
            meal_plan[day] = {
                "lunch": (
                    recipes[i * recipes_per_day]
                    if i * recipes_per_day < len(recipes)
                    else None
                ),
                "dinner": (
                    recipes[i * recipes_per_day + 1]
                    if i * recipes_per_day + 1 < len(recipes)
                    else None
                ),
            }
    else:
        # Recalculate remaining recipes from the given meal plan
        all_recipes = Recipe.get_recipes_for_user_id(
            user_id=user_id
        ) + Recipe.get_shared_recipes(shared_with_user_id=user_id)
        used_recipe_ids = [
            meal["recipe_id"]
            for day_meals in meal_plan_saved.values()
            for meal in day_meals.values()
            if meal
        ]
        remaining_recipes = [
            recipe
            for recipe in all_recipes
            if recipe["recipe_id"] not in used_recipe_ids
        ]
        meal_plan = meal_plan_saved
        meal_plan_saved = None

    return render_template(
        "app/mealplan.html", meal_plan=meal_plan, remaining_recipes=remaining_recipes
    )
