from flask import Blueprint, render_template
from flask_login import current_user
import os

bp = Blueprint("home", __name__)


@bp.route("/")
def index():
    if current_user.is_authenticated:
        return render_template(
            "app/home.html",
            user=current_user.user_name,
            email=current_user.user_email,
        )
    else:
        local_user = os.getenv("LOCAL_USER")
        return render_template("app/index.html", local_user=local_user)
