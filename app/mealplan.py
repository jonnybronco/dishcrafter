from datetime import datetime
from app.db import get_db
import json


class MealPlan:
    @staticmethod
    def save_current_mealplan(user_id, mealplan_data):
        db = get_db()
        mealplan_json = json.dumps(
            mealplan_data
        )  # Convert the mealplan data to a JSON string

        # Assuming that plan_date is the same for the whole meal plan
        plan_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        db.execute(
            """
            INSERT INTO meal_plans (user_id, mealplan_data, plan_date) 
            VALUES (?, ?, ?)
            """,
            (user_id, mealplan_json, plan_date),
        )
        db.commit()

    @staticmethod
    def get_last_mealplan(user_id):
        db = get_db()
        cursor = db.execute(
            """
            SELECT mealplan_data
            FROM meal_plans
            WHERE user_id = ?
            ORDER BY creation_date DESC
            LIMIT 1
            """,
            (user_id,),
        )
        row = cursor.fetchone()
        if row:
            mealplan_data = json.loads(
                row["mealplan_data"]
            )  # Convert JSON string back to a Python dict
            return mealplan_data
        return None
