/** @type {import('tailwindcss').Config} */
export default {
  content: ['./**/*.{html,js}'],
  theme: {
    extend: {
      fontSize: {
        xxs: '0.625rem', // 10px
        xxxs: '0.5rem' // 8px
      },
      colors: {
        'drag-over-bg': '#e8a87c',
        'drag-over-text': '#eaeaea'
      }
    }
  },
  plugins: []
};
