from app.db import get_db


class Recipe:
    @staticmethod
    def create(**recipe_data):
        db = get_db()
        cursor = db.execute(
            """
        INSERT INTO recipes (user_id, title, description, portion_serves, note, web_link, image_url) 
        VALUES (?, ?, ?, ?, ?, ?,?)
        """,
            (
                recipe_data["user_id"],
                recipe_data["title"],
                recipe_data["description"],
                recipe_data["portion_serves"],
                recipe_data["note"],
                recipe_data["web_link"],
                recipe_data["image_url"],
            ),
        )
        recipe_id = cursor.lastrowid
        for ingredient in recipe_data["ingredients"]:
            cursor.execute(
                "SELECT ingredient_id FROM ingredients WHERE ingredient_type = ? AND user_id = ?",
                (ingredient["ingredient_type"], recipe_data["user_id"]),
            )
            result = cursor.fetchone()
            if result:
                ingredient_id = result[0]
            else:
                # Insert new ingredient
                cursor.execute(
                    """
                    INSERT INTO ingredients (unit, ingredient_type, user_id) 
                    VALUES (?, ?, ?)
                    """,
                    (
                        ingredient["unit"],
                        ingredient["ingredient_type"],
                        recipe_data["user_id"],
                    ),
                )
                ingredient_id = cursor.lastrowid
            # Link ingredient to recipe
            cursor.execute(
                """
                INSERT INTO recipe_ingredients (recipe_id, ingredient_id, amount) 
                VALUES (?, ?, ?)
                """,
                (recipe_id, ingredient_id, ingredient["amount"]),
            )
        db.commit()
        return recipe_id

    @staticmethod
    def update(
        user_id: int,
        recipe_id: str,
        title: str,
        description: str,
        ingredients: list,
        portion_serves: int,
        note: str,
        web_link: str,
        image_url: str,
    ):
        db = get_db()
        # Update basic recipes parameter
        db.execute(
            """
            UPDATE recipes 
            SET title = ?, description = ?, portion_serves = ?, note = ?, web_link = ?, image_url = ?
            WHERE recipe_id = ?
            """,
            (title, description, portion_serves, note, web_link, image_url, recipe_id),
        )

        # Step 1: Collect all current ingredient IDs for this recipe
        cursor = db.execute(
            "SELECT ingredient_id FROM recipe_ingredients WHERE recipe_id = ?",
            (recipe_id,),
        )
        current_ingredient_ids = set(
            ingredient_id for ingredient_id, in cursor.fetchall()
        )

        # Keep track of which ingredient IDs are still used
        used_ingredient_ids = set()

        # Step 2: Update and add ingredients
        # Assuming recipe_data['ingredients'] is a list of ingredient dictionaries
        # Each ingredient dictionary should have 'ingredient_type', 'unit', and 'amount'
        for ingredient in ingredients:
            # Check if the ingredient already exists
            cursor = db.execute(
                """
                SELECT ingredient_id FROM ingredients 
                WHERE ingredient_type = ? AND unit = ? AND user_id = ?
                """,
                (
                    ingredient["ingredient_type"],
                    ingredient["unit"],
                    user_id,
                ),
            )
            result = cursor.fetchone()

            if result:
                ingredient_id = result[0]
            else:
                # Insert new ingredient into ingredients table
                cursor.execute(
                    """
                    INSERT INTO ingredients (unit, ingredient_type, user_id) 
                    VALUES (?, ?, ?)
                    """,
                    (
                        ingredient["unit"],
                        ingredient["ingredient_type"],
                        user_id,
                    ),
                )
                ingredient_id = cursor.lastrowid
            used_ingredient_ids.add(ingredient_id)
            # Update or insert into recipe_ingredients table
            cursor.execute(
                """
                INSERT INTO recipe_ingredients (recipe_id, ingredient_id, amount) 
                VALUES (?, ?, ?) ON CONFLICT(recipe_id, ingredient_id) DO UPDATE 
                SET amount = EXCLUDED.amount
                """,
                (
                    recipe_id,
                    ingredient_id,
                    ingredient["amount"],
                ),
            )

        # Step 3: Remove ingredients not in the updated list
        ingredients_to_remove = current_ingredient_ids - used_ingredient_ids
        for ingredient_id in ingredients_to_remove:
            cursor.execute(
                "DELETE FROM recipe_ingredients WHERE recipe_id = ? AND ingredient_id = ?",
                (recipe_id, ingredient_id),
            )
        db.commit()
        return recipe_id

    @staticmethod
    def delete(recipe_id, user_id):
        db = get_db()
        cursor = db.cursor()

        # Check if the recipe exists and belongs to the user
        cursor.execute(
            "SELECT 1 FROM recipes WHERE recipe_id = ? AND user_id = ?",
            (recipe_id, user_id),
        )
        recipe_exists = cursor.fetchone()

        if not recipe_exists:
            # Recipe does not exist or does not belong to the user
            return False

        # Step 1: Delete associations in recipe_ingredients
        cursor.execute(
            "DELETE FROM recipe_ingredients WHERE recipe_id = ?", (recipe_id,)
        )

        # Step 2: Delete the recipe itself
        cursor.execute(
            "DELETE FROM recipes WHERE recipe_id = ? AND user_id = ?",
            (recipe_id, user_id),
        )

        # Step 3: Optionally delete unused ingredients specific to this user
        # First, find ingredients that are no longer linked to any recipes
        cursor.execute(
            """
        SELECT i.ingredient_id
        FROM ingredients i
        LEFT JOIN recipe_ingredients ri ON i.ingredient_id = ri.ingredient_id
        WHERE i.user_id = ? AND ri.ingredient_id IS NULL
        """,
            (user_id,),
        )

        unused_ingredient_ids = cursor.fetchall()

        # Delete these unused ingredients
        for ingredient_id in unused_ingredient_ids:
            cursor.execute(
                "DELETE FROM ingredients WHERE ingredient_id = ? AND user_id = ?",
                (ingredient_id[0], user_id),
            )
        db.commit()
        return True

    @staticmethod
    def get_recipes_for_user_id(user_id):
        db = get_db()
        rows = db.execute(
            """
            SELECT r.recipe_id, r.user_id, r.title, r.description, r.portion_serves, r.note, r.web_link, r.image_url,
                i.ingredient_type, ri.amount, i.unit
            FROM recipes r
            JOIN recipe_ingredients ri ON r.recipe_id = ri.recipe_id
            JOIN ingredients i ON ri.ingredient_id = i.ingredient_id
            WHERE r.user_id = ?
            ORDER BY r.recipe_id
            """,
            (user_id,),
        ).fetchall()

        recipes = {}
        for row in rows:
            recipe_id = row[0]  # Correctly use the recipe_id as the unique identifier

            if recipe_id not in recipes:
                recipes[recipe_id] = {
                    "recipe_id": row[0],
                    "user_id": row[1],
                    "title": row[2],
                    "description": row[3],
                    "portion_serves": row[4],
                    "note": row[5],
                    "web_link": row[6],
                    "image_url": row[7],
                    "ingredients": [],
                }

            ingredient = {"ingredient_type": row[8], "amount": row[9], "unit": row[10]}
            recipes[recipe_id]["ingredients"].append(ingredient)

        return list(recipes.values())

    @staticmethod
    def get_recipe_by_id(recipe_id):
        db = get_db()
        rows = db.execute(
            """
            SELECT r.recipe_id, r.user_id, r.title, r.description, r.portion_serves, r.note, r.web_link, r.image_url, i.ingredient_type, ri.amount, i.unit
            FROM recipes r
            JOIN recipe_ingredients ri ON r.recipe_id = ri.recipe_id
            JOIN ingredients i ON ri.ingredient_id = i.ingredient_id
            WHERE r.recipe_id = ?
            """,
            (recipe_id,),
        ).fetchall()
        if rows:
            recipe = {
                "recipe_id": rows[0][0],
                "user_id": rows[0][1],
                "title": rows[0][2],
                "description": rows[0][3],
                "portion_serves": rows[0][4],
                "note": rows[0][5],
                "web_link": rows[0][6],
                "image_url": rows[0][7],
                "ingredients": [],
            }

            for row in rows:
                ingredient = {
                    "ingredient_type": row[8],
                    "amount": row[9],
                    "unit": row[10],
                }
                recipe["ingredients"].append(ingredient)
            return recipe
        else:
            return None

    @staticmethod
    def share_recipe(recipe_id, owner_user_id, shared_with_user_id):
        db = get_db()
        cursor = db.cursor()
        # Check for existing shared recipe
        cursor.execute(
            """
            SELECT 1 FROM shared_recipes 
            WHERE recipe_id = ? AND owner_user_id = ? AND shared_with_user_id = ?
            """,
            (recipe_id, owner_user_id, shared_with_user_id),
        )
        existing_shared_recipe = cursor.fetchone()
        if not existing_shared_recipe:
            cursor.execute(
                """
                INSERT INTO shared_recipes (recipe_id, owner_user_id, shared_with_user_id)
                VALUES (?, ?, ?)
                """,
                (recipe_id, owner_user_id, shared_with_user_id),
            )
            db.commit()

    @staticmethod
    def get_shared_recipes(shared_with_user_id):
        db = get_db()
        cursor = db.cursor()
        cursor.execute(
            """
            SELECT sr.recipe_id, r.title, r.description, r.user_id
            FROM shared_recipes sr
            JOIN recipes r ON sr.recipe_id = r.recipe_id
            WHERE sr.shared_with_user_id = ?
            """,
            (shared_with_user_id,),
        )
        shared_recipes = cursor.fetchall()
        return [Recipe.get_recipe_by_id(recipe[0]) for recipe in shared_recipes]
