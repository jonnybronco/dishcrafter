from app import create_app
import os

if __name__ == "__main__":
    app = create_app()
    # Check if running in production environment
    if os.environ.get("FLASK_ENV") == "development":
        # Local development run: with SSL context
        app.run(host="0.0.0.0", debug=True, port="5000", ssl_context="adhoc")
    else:
        # Production run: without SSL context
        app.run(host="0.0.0.0", debug=True, port="5000")
