import { getWeekNumber, getWeekDates } from './dateFunctions.js';
import { getWeekOffset } from './stateManager.js';
import { changeBackgroundColor } from './uiFunctions.js';
import { createNoRecipeWrapper } from './miscFunctions.js';
import { setupDragAndDrop } from './eventHandlers.js';

export function displayDates() {
  var dates = getWeekDates(getWeekOffset());
  for (var day in dates) {
    var dateString = dates[day].toLocaleDateString('de-DE', {
      day: 'numeric',
      month: 'long'
    });
    document.getElementById(day + '-date').innerText = dateString;
  }
}

// Define the function to take a screenshot
export function takeScreenshot() {
  var weekElement = document.querySelector('#week');

  // Log the weekElement to check if it exists
  console.log('Week Element:', weekElement);

  // Store the original styles
  var originalStyles = {
    width: weekElement.style.width,
    overflow: weekElement.style.overflow
  };

  // Adjust the styles to ensure the full content is visible
  weekElement.style.width = '1050px'; // or a fixed width that fits all content
  weekElement.style.overflow = 'visible'; // Ensure no content is hidden

  // Ensure images are loaded
  setTimeout(() => {
    if (weekElement) {
      html2canvas(weekElement, {
        onclone: function (documentClone) {
          // This function can help ensure styles are properly cloned
          // Modify background images if needed
        }
      })
        .then(function (canvas) {
          var link = document.createElement('a');
          link.href = canvas.toDataURL('image/png');
          var date = new Date();
          var dateString =
            date.getFullYear().toString() +
            (date.getMonth() + 1).toString().padStart(2, '0') +
            date.getDate().toString().padStart(2, '0');
          link.download = dateString + '_mealplan.png';
          link.click();
        })
        .catch(function (error) {
          console.error('html2canvas error:', error);
        })
        .finally(() => {
          // Restore the original styles after the screenshot is taken
          weekElement.style.width = originalStyles.width;
          weekElement.style.overflow = originalStyles.overflow;
        });
    } else {
      console.error('The element with the class "week" was not found.');
    }
  }, 1000); // Delay of 1 second before taking screenshot
}

// Define the function which extracts the defined menus from the
// mealplan and sends it to the get_shopping_list endpoint.
export function gatherMealplanMenus() {
  let menuTitles = [];
  let menus = document.querySelectorAll('.draggable');

  menus.forEach(function (menu) {
    if (menu.closest('#remaining-menus') === null) {
      menuTitles.push(menu.getAttribute('recipe-id'));
    }
  });

  // Use fetch, axios or XMLHttpRequest to send data to Python
  fetch('/shoppinglist/get', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ titles: menuTitles })
  })
    .then(response => response.json())
    .then(data => {
      if (data.status === 'success') {
        window.location.href = `/shoppinglist/overview/${userUUID}`;
      }
    });
}

export function saveCurrentMealplan() {
  let menuItems = [];
  let menus = document.querySelectorAll('.draggable');

  menus.forEach(function (menu) {
    if (menu.closest('#remaining-menus') === null) {
      let recipeId = menu.getAttribute('recipe-id');
      let day = menu.getAttribute('data-day');
      let meal = menu.getAttribute('data-meal');

      menuItems.push({
        recipeId: recipeId,
        day: day,
        meal: meal
      });
    }
  });

  // Use fetch, axios or XMLHttpRequest to send data to Python
  fetch('/mealplan/saveCurrent', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ menuItems: menuItems })
  })
    .then(response => response.json())
    .then(data => {
      if (data.status === 'success') {
        window.location.href = `/mealplan`;
      }
    });
}

export function loadLastMealplan() {
  window.location.href = `/mealplan/load_last_mealplan`;
}

export function moveToRemaining(day) {
  const remainingMenus = document.getElementById('remaining-menus');
  const meals = ['lunch', 'dinner'];

  meals.forEach(meal => {
    const draggedElem = document.getElementById(`${day}-${meal}-text`);
    const placeholder = createNoRecipeWrapper(draggedElem);
    placeholder.querySelector('.draggable').innerText = draggedElem.innerText;
    // Get background-image from the parent of the dragged element
    var parentElem = draggedElem.parentElement;
    var backgroundImageUrl =
      window.getComputedStyle(parentElem).backgroundImage;
    placeholder.style.backgroundImage = backgroundImageUrl;
    remainingMenus.appendChild(placeholder);

    // Update the dragged element's text and background color
    draggedElem.innerText = 'Nix geplant!';
    draggedElem.removeAttribute('recipe-id');
    changeBackgroundColor(draggedElem, 'Nix geplant!', '#e8a87c');
    // As some new elements were created re-run eventhandler setup
    setupDragAndDrop();
  });
}

export function clearAll() {
  // Select the div with id "week"
  const weekDiv = document.getElementById('week');
  // Select all elements within the week div that have a data-day attribute
  const elementsWithDay = weekDiv.querySelectorAll('[data-day]');
  // Extract the data-day attributes and use a Set to ensure uniqueness
  const days = Array.from(
    new Set(
      Array.from(elementsWithDay).map(element =>
        element.getAttribute('data-day')
      )
    )
  );
  days.forEach(day => {
    moveToRemaining(day);
  });
}
