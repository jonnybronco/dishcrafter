export function changeBackgroundColor(elem, text, color) {
  // Change the element's background color if it matches the given text
  if (elem.innerText === text) {
    elem.style.backgroundColor = color;
    elem.style.borderRadius = '5px';
    elem.style.padding = '5px';
    elem.parentNode.style.backgroundColor = color; // Color the parent element too
    elem.parentNode.style.backgroundImage = 'none';
  } else {
    elem.style.backgroundColor = ''; // Reset to default if not matching the text
    elem.parentNode.style.backgroundColor = ''; // Reset the parent as well
  }
}

export function addHighlight(event) {
  var dropTarget = event.target.closest('.meal-item');
  if (dropTarget) {
    dropTarget.classList.remove('bg-slate-800');
    dropTarget.classList.add('bg-slate-400');
  } else if (event.target.id === 'remaining-menus-title') {
    event.target.classList.add('bg-slate-400');
  }
}

export function removeHighlight(event) {
  var dropTarget = event.target.closest('.meal-item');

  // Check if the new target is a child of the current target
  if (dropTarget && !dropTarget.contains(event.relatedTarget)) {
    dropTarget.classList.remove('bg-slate-400');
    dropTarget.classList.add('bg-slate-800');
  } else if (event.target.id === 'remaining-menus-title') {
    event.target.classList.remove('bg-slate-400');
  }
}

export function setDraggable(element) {
  // make element draggable
  element.setAttribute('draggable', 'true');
  element.setAttribute('ondragstart', 'drag(event)');
}
