export function createNoRecipeWrapper(draggedElem) {
  const dimDiv = document.createElement('div');
  dimDiv.classList.add('absolute', 'inset-0', 'bg-black', 'opacity-50');
  // Create 'No recipe' element
  var noRecipeElem = document.createElement('div');
  noRecipeElem.classList.add('relative', 'draggable');
  noRecipeElem.setAttribute('draggable', 'true');
  noRecipeElem.setAttribute('recipe-id', draggedElem.getAttribute('recipe-id'));
  noRecipeElem.setAttribute(
    'id',
    'recipe_id_' + draggedElem.getAttribute('recipe-id')
  );
  noRecipeElem.innerText = 'Nix geplant!';

  // Create wrapper for 'No recipe' element
  var wrapperDiv = document.createElement('div');
  wrapperDiv.classList.add(
    'meal-item',
    'drop-target',
    'min-w-[112px]',
    'h-[112px]',
    'relative'
  );
  wrapperDiv.style.backgroundImage = draggedElem.style.backgroundImage;
  wrapperDiv.style.backgroundSize = 'cover';
  wrapperDiv.style.backgroundPosition = 'center';
  wrapperDiv.appendChild(dimDiv);
  wrapperDiv.appendChild(noRecipeElem);
  return wrapperDiv;
}
