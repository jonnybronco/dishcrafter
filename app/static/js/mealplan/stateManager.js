let weekOffset = 0; // Initially, no offset

export function incrementWeekOffset() {
  weekOffset++;
}

export function decrementWeekOffset() {
  weekOffset--;
}

export function getWeekOffset() {
  return weekOffset;
}
