import { createNoRecipeWrapper } from './miscFunctions.js';
import {
  changeBackgroundColor,
  setDraggable,
  removeHighlight
} from './uiFunctions.js';

// If an element is dragged
export function drag(event) {
  event.dataTransfer.setData('text', event.target.id);
}

export function allowDrop(event) {
  event.preventDefault();
}

export function drop(event) {
  event.preventDefault();
  // Get the id of the element that was dropped
  var data = event.dataTransfer.getData('text');
  // get the element with the id from data
  var draggedElem = document.getElementById(data);
  // Select the element where the drag-element was dropped onto, this can either be a meal-item,
  // e.g. in the Week Lunch/Dinner zones, or on the remaining zones.
  var target =
    event.target.closest('.meal-item') || event.target.closest('.drop-target');

  // If the target has the class drag-over remove it, such that the css is handled.
  target.classList.remove('drag-over');
  removeHighlight(event);

  // TODO: Fix for remaining-menus-drop-zone!!
  if (target.id === 'remaining-menus-title') {
    // Create a placeholder for the dragged element, where the inner text remains, but
    // the wrapper defines a menu in the reamaining-menus zone.
    var placeholder = createNoRecipeWrapper(draggedElem);
    placeholder.querySelector('.draggable').innerText = draggedElem.textContent;
    // Get background-image from the parent of the dragged element
    var parentElem = draggedElem.parentElement;
    var backgroundImageUrl =
      window.getComputedStyle(parentElem).backgroundImage;
    placeholder.style.backgroundImage = backgroundImageUrl;
    // Append recipe to the remaining recipes list
    document.getElementById('remaining-menus').appendChild(placeholder);
    // target.appendChild(placeholder);
    // Clean up the drag element, set text, remove recipe-id etc.
    draggedElem.innerText = 'Nix geplant!';
    draggedElem.setAttribute('recipe-id', 'None');
    parentElem.style.backgroundImage = 'none';
    setDraggable(draggedElem);
    changeBackgroundColor(draggedElem, 'Nix geplant!', '#64748b');
  } else {
    var targetElem = target.querySelector('.draggable');
    var temp_recipe_id = draggedElem.getAttribute('recipe-id');
    var temp = draggedElem.textContent;
    var temp_background_image = draggedElem.parentElement.style.backgroundImage;
    // Swap the text content
    draggedElem.textContent = targetElem.textContent;
    draggedElem.parentElement.style.backgroundImage =
      targetElem.parentElement.style.backgroundImage;
    draggedElem.setAttribute('recipe-id', targetElem.getAttribute('recipe-id'));

    targetElem.setAttribute('recipe-id', temp_recipe_id);
    targetElem.textContent = temp;
    targetElem.parentElement.style.backgroundImage = temp_background_image;
    // Update background colors based on text content
    changeBackgroundColor(draggedElem, 'Nix geplant!', '#64748b');
    changeBackgroundColor(targetElem, 'Nix geplant!', '#64748b');
  }
}
