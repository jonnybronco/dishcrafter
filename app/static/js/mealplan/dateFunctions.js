export function getWeekNumber(d) {
  d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
  d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
  var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
  var weekNo = Math.ceil(((d - yearStart) / 86400000 + 1) / 7);
  return weekNo;
}

export function getWeekDates() {
  const now = new Date();
  const todayIndex = now.getDay(); // Sunday - 0, Monday - 1, ..., Saturday - 6

  const dayNames = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  const dates = {};

  for (let i = 0; i < 7; i++) {
    const currentDayIndex = (todayIndex + i) % 7;
    const currentDate = new Date(now);
    currentDate.setDate(now.getDate() + i);

    dates[dayNames[currentDayIndex]] = currentDate;
  }

  return dates;
}
