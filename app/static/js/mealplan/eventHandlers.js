import {
  displayDates,
  takeScreenshot,
  gatherMealplanMenus,
  moveToRemaining,
  clearAll,
  saveCurrentMealplan,
  loadLastMealplan
} from './domFunctions.js';
import { drag, allowDrop, drop } from './ddFunctions.js';
import { incrementWeekOffset, decrementWeekOffset } from './stateManager.js';
import { addHighlight, removeHighlight } from './uiFunctions.js';

export function setupScreenShotButton() {
  document
    .getElementById('screenshotButton')
    .addEventListener('click', takeScreenshot);
}

export function setupClearAllButton() {
  document.getElementById('clearAllButton').addEventListener('click', clearAll);
}

export function setupSaveCurrentMealplan() {
  document
    .getElementById('saveCurrentMealplanButton')
    .addEventListener('click', saveCurrentMealplan);
}

export function setupLoadLastMealplan() {
  document
    .getElementById('loadLastMealplanButton')
    .addEventListener('click', loadLastMealplan);
}

export function setupShoppingListButton() {
  document
    .getElementById('getShoppingList')
    .addEventListener('click', gatherMealplanMenus);
}

export function setupClearRecipeButtons() {
  const moveToRemainingButtons =
    document.querySelectorAll('#move-to-remaining');
  moveToRemainingButtons.forEach(button => {
    button.addEventListener('click', function (event) {
      const day = event.target.getAttribute('data-day');
      moveToRemaining(day);
    });
  });
}

export function setupDragAndDrop() {
  const draggableElements = document.querySelectorAll('[draggable="true"]');

  draggableElements.forEach(elem => {
    elem.addEventListener('dragstart', drag);
  });
  const dropTargetElements = document.querySelectorAll('.drop-target');
  dropTargetElements.forEach(elem => {
    elem.addEventListener('drop', drop);
    elem.addEventListener('dragover', allowDrop);
    elem.addEventListener('dragenter', addHighlight);
    elem.addEventListener('dragleave', removeHighlight);
  });
}

// Setup event listeners when the DOM is fully loaded
document.addEventListener('DOMContentLoaded', function () {
  setupClearRecipeButtons();
  setupScreenShotButton();
  setupShoppingListButton();
  setupDragAndDrop();
  displayDates();
  setupClearAllButton();
  setupSaveCurrentMealplan();
  setupLoadLastMealplan();
});
