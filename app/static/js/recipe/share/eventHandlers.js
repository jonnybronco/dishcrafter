import {
  toggleCardSelection,
  selectAllCards,
  updateNextButtonState,
  getSelectedRecipeIds
} from './domFunctions.js';

export function setupSelectAndShareButtons() {
  const selectButton = document.getElementById('selectShareRecipe');
  const shareAllButton = document.getElementById('shareAllRecipes');
  const nextButton = document.getElementById('next-btn');
  const cards = document.querySelectorAll('.card');

  // Initialize the Next button as disabled
  updateNextButtonState(cards, nextButton);

  // Toggle 'selected' class on card click and update Next button state
  cards.forEach(card => {
    card.addEventListener('click', () => {
      if (selectButton.textContent.includes('Select')) {
        toggleCardSelection(card);
        updateNextButtonState(cards, nextButton);
        console.log('Card clicked:', card);
        console.log('Selected Recipe IDs:', getSelectedRecipeIds());
      }
    });
  });

  // Handle the Select/Deselect All functionality
  selectButton.addEventListener('click', () => {
    const isSelectMode = selectButton.textContent === 'Select';
    selectButton.textContent = isSelectMode ? 'Deselect' : 'Select';
    selectAllCards(cards, isSelectMode);
    updateNextButtonState(cards, nextButton);
    console.log('Select button clicked:', selectButton.textContent);
  });

  // Handle the Share All functionality
  shareAllButton.addEventListener('click', () => {
    const shouldSelectAll = !Array.from(cards).every(card =>
      card.classList.contains('ring')
    );
    selectAllCards(cards, shouldSelectAll);
    updateNextButtonState(cards, nextButton);
    console.log('Share All button clicked');
  });
}

export function setupNextButton() {
  const nextButton = document.getElementById('next-btn');
  const form = document.getElementById('shareForm');

  nextButton.addEventListener('click', () => {
    console.log('Next button clicked');
    const userIdInput = document.getElementById('userIdInput').value;
    const selectedRecipeIds = getSelectedRecipeIds();
    console.log('User ID:', userIdInput);
    console.log('Selected Recipe IDs:', selectedRecipeIds);

    if (selectedRecipeIds.length > 0 && userIdInput) {
      fetch('/recipe/sharewith', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          recipeIds: selectedRecipeIds,
          userId: userIdInput
        })
      })
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then(data => {
          console.log(data); // Log the success message or handle it as needed
          // Redirect to /recipes/share
          window.location.href = '/recipe/share';
        })
        .catch(error => {
          console.error(
            'There was a problem with your fetch operation:',
            error
          );
        });
    } else {
      alert('Please select recipes and enter a User ID.');
    }
  });

  form.addEventListener('submit', event => {
    event.preventDefault(); // Prevent the default form submission
  });
}

// Setup event listeners when the DOM is fully loaded
document.addEventListener('DOMContentLoaded', function () {
  setupSelectAndShareButtons();
  setupNextButton();
});
