export function toggleCardSelection(card) {
  card.classList.toggle('ring');
}

export function selectAllCards(cards, isSelected) {
  cards.forEach(card => card.classList.toggle('ring', isSelected));
}

export function updateNextButtonState(cards, nextButton) {
  const anySelected = Array.from(cards).some(card =>
    card.classList.contains('ring')
  );
  nextButton.classList.toggle('opacity-50', !anySelected);
  nextButton.classList.toggle('cursor-not-allowed', !anySelected);
}

export function getSelectedRecipeIds() {
  const selectedCards = document.querySelectorAll('.card.ring');
  const selectedIds = Array.from(selectedCards).map(
    card => card.querySelector('p').textContent
  );
  return selectedIds;
}
