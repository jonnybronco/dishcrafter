export function setupSearchFunctionality() {
  const searchInput = document.getElementById('recipeSearchInput');
  const recipeCards = document.querySelectorAll('.card');

  searchInput.addEventListener('input', () => {
    const searchText = searchInput.value.toLowerCase();

    recipeCards.forEach(card => {
      const title = card.querySelector('.card-title').textContent.toLowerCase();
      const description = card
        .querySelector('.description')
        .textContent.toLowerCase();

      // You can extend this to search within ingredients or other fields
      if (title.includes(searchText) || description.includes(searchText)) {
        card.style.display = '';
      } else {
        card.style.display = 'none';
      }
    });
  });
}

export function setupShoppinglistButton() {
  const shoppingListButtons = document.querySelectorAll('.btn-shoppinglist');

  shoppingListButtons.forEach(button => {
    button.addEventListener('click', event => {
      event.preventDefault(); // Prevent form submission if it's in a form

      const recipeId = button.dataset.recipeId; // Assuming the button has data-recipe-id attribute

      fetch('/shoppinglist/get', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ titles: [recipeId] }) // Send the recipeId in an array
      })
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then(data => {
          if (data.status === 'success') {
            window.location.href = `/shoppinglist/overview/${userUUID}`;
          }
        })
        .catch(error => {
          console.error('Error:', error);
        });
    });
  });
}

// Setup event listeners when the DOM is fully loaded
document.addEventListener('DOMContentLoaded', function () {
  setupSearchFunctionality();
  setupShoppinglistButton();
});
