let ingredientIndex;

export function initializeIngredientIndex() {
  ingredientIndex =
    parseInt(
      document
        .getElementById('ingredientFields')
        .getAttribute('data-ingredient-count')
    ) || 1;
}

export function addIngredientRow() {
  if (ingredientIndex === undefined) {
    initializeIngredientIndex();
  }
  const ingredientFields = document.getElementById('ingredientFields');
  const ingredientRow = createIngredientRow(ingredientIndex);
  ingredientFields.appendChild(ingredientRow);
  ingredientIndex++;
}

export function resetIngredientIndex() {
  ingredientIndex = 1;
}

export function createIngredientRow(index) {
  const ingredientRow = document.createElement('div');
  ingredientRow.classList.add('ingredientContainer');
  ingredientRow.id = 'ingredientRow' + index;

  ingredientRow.innerHTML = `
    <div class="ingredientRow flex items-center space-x-4 py-2">
      <div class="w-1/3">
        <input type="number" class="text-input w-full" name="ingredient_amount[${index}]" placeholder="Amount" required />
      </div>
      <div class="w-1/3">
        <input type="text" class="text-input w-full" name="ingredient_unit[${index}]" placeholder="Unit" required />
      </div>
      <div class="w-1/3">
        <input type="text" class="text-input w-full" name="ingredient_type[${index}]" placeholder="Type" required />
      </div>
      <div class="w-1/4 text-right">
        <button class="btn-delete rounded-md bg-red-600 hover:bg-red-400 px-3 py-2 text-sm font-medium text-white" data-index="${index}">x</button>
      </div>
    </div>
  `;

  return ingredientRow;
}

export function deleteRow(index) {
  const row = document.getElementById('ingredientRow' + index);
  if (row) {
    row.remove();
    ingredientIndex--;
    // Re-index the rows and inputs
    for (let i = index + 1; i <= ingredientIndex; i++) {
      const row = document.getElementById('ingredientRow' + i);
      row.id = 'ingredientRow' + (i - 1);
      row.querySelectorAll('.btn-delete').forEach(button => {
        button.setAttribute('data-index', i - 1);
      });
      row.querySelectorAll('input').forEach(input => {
        const name = input.name.replace(`[${i}]`, `[${i - 1}]`);
        input.name = name;
      });
    }
  }
}

export function validateInput(input) {
  const textPattern = /^[a-zA-Z0-9 äöüÄÖÜ\- ]+$/;
  if (input.value === '') {
    return true;
  }
  if (!textPattern.test(input.value)) {
    alert('Invalid input: ' + input.value);
    input.focus();
    return false;
  }
  return true;
}
