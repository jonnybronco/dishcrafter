import { addIngredientRow, deleteRow, validateInput } from './domFunctions.js';

export function setupEventListeners() {
  // Listener for adding an ingredient row
  document
    .getElementById('addIngredient')
    .addEventListener('click', function () {
      addIngredientRow();
    });

  // Event delegation for delete buttons in dynamically added ingredient rows
  document
    .getElementById('ingredientFields')
    .addEventListener('click', function (event) {
      // Check if the clicked element is a delete button
      if (event.target.classList.contains('btn-delete')) {
        event.preventDefault(); // Prevent form submission

        // Extract the row index from the button's data-index attribute
        const index = parseInt(event.target.getAttribute('data-index'), 10);
        deleteRow(index);
      }
    });

  // Event delegation for input validation in dynamically added ingredient rows
  document
    .getElementById('ingredientFields')
    .addEventListener('change', function (event) {
      // Check if the changed element is one of the inputs that requires validation
      if (event.target.classList.contains('form-input')) {
        validateInput(event.target);
      }
    });
}

// Setup event listeners when the DOM is fully loaded
document.addEventListener('DOMContentLoaded', function () {
  setupEventListeners();
});
