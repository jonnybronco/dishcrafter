import uuid
from flask_login import UserMixin
from app.db import get_db


class User(UserMixin):
    def __init__(
        self, id=None, google_id=None, user_name=None, user_email=None, user_uuid=None
    ):
        self.id = id
        self.google_id = google_id
        self.user_name = user_name
        self.user_email = user_email
        self.user_uuid = user_uuid

    @staticmethod
    def get_by_user_id(id):
        db = get_db()
        user = db.execute("SELECT * FROM users WHERE user_id = ?", (id,)).fetchone()
        if not user:
            return None

        user = User(
            id=user[0],
            google_id=user[1],
            user_name=user[2],
            user_email=user[3],
            user_uuid=user[4],
        )
        return user

    @staticmethod
    def get_by_google_id(google_id):
        db = get_db()
        user = db.execute(
            "SELECT * FROM users WHERE google_id = ?", (google_id,)
        ).fetchone()
        if not user:
            return None

        user = User(
            id=user[0],
            google_id=user[1],
            user_name=user[2],
            user_email=user[3],
            user_uuid=user[4],
        )
        return user

    @staticmethod
    def create(google_id, user_name, user_email):
        db = get_db()
        cursor = db.execute(
            "INSERT INTO users (google_id, username, email, user_uuid) "
            "VALUES (?, ?, ?, ?)",
            (google_id, user_name, user_email, str(uuid.uuid4())),
        )
        db.commit()
        return cursor.lastrowid
