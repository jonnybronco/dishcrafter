from app.db import get_db


class Ingredient:
    def __init__(self, id=None, amount=None, unit=None, ingredient_type=None):
        self.id = id
        self.unit = unit
        self.ingredient_type = ingredient_type

    @staticmethod
    def create(unit, ingredient_type, user_id):
        db = get_db()
        cursor = db.execute(
            "INSERT INTO ingredients (unit, ingredient_type, user_id) "
            "VALUES (?, ?, ?)",
            (unit, ingredient_type, user_id),
        )
        db.commit()
        return cursor.lastrowid

    @staticmethod
    def get_by_type(ingredient_type):
        db = get_db()
        ingredients = db.execute(
            "SELECT * FROM ingredients WHERE ingredient_type = ?", (ingredient_type,)
        ).fetchall()
        return ingredients

    @staticmethod
    def get_by_id(id):
        db = get_db()
        ingredient = db.execute(
            "SELECT * FROM ingredients WHERE ingredient_id = ?", (id,)
        ).fetchone()
        return ingredient

    def update(id, unit=None, ingredient_type=None):
        db = get_db()
        db.execute(
            "UPDATE ingredients SET unit = ?, ingredient_type = ? WHERE ingredient_id = ?",
            (unit, ingredient_type, id),
        )
        db.commit()

    @staticmethod
    def delete_by_id(id):
        db = get_db()
        db.execute("DELETE FROM ingredients WHERE ingredient_id = ?", (id,))
        db.commit()
