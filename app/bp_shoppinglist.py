from app.recipe import Recipe
from collections import defaultdict
from flask import Blueprint, request, jsonify, session, render_template

bp = Blueprint("shoppinglist", __name__, url_prefix="/shoppinglist")
FINAL_INGREDIENTS_GLOBAL = None


@bp.route("/get", methods=["POST"])
def get():
    global FINAL_INGREDIENTS_GLOBAL
    FINAL_INGREDIENTS_GLOBAL = None
    data = request.json
    # There needs to be a better way for this!!!
    # The problem is, the following view, /overview needs to be
    # publicly accessible, for Bring, such that Bring can retrieve
    # the shoppinglist. For this the recipes, e.g. ingredients of them
    # need to be stored in a global, as
    # the session cookie is to small to hold the information.
    collected_recipes = data["titles"]
    all_recipes = Recipe.get_recipes_for_user_id(
        session.get("_user_id")
    ) + Recipe.get_shared_recipes(session.get("_user_id"))
    if type(collected_recipes) == list and len(collected_recipes) > 0:
        matching_recipes = [
            recipe
            for recipe in all_recipes
            if str(recipe["recipe_id"]) in collected_recipes
        ]
        # Dictionary to store summed-up ingredients
        summed_ingredients = defaultdict(lambda: {"amount": 0, "unit": None})
        # Sum up ingredients
        for recipe in matching_recipes:
            for ingredient in recipe["ingredients"]:
                key = ingredient["ingredient_type"]
                unit = ingredient["unit"]
                amount = ingredient["amount"]

                # Sumup ingredients
                if summed_ingredients[key]["unit"] is None:
                    summed_ingredients[key]["unit"] = unit

                if summed_ingredients[key]["unit"] == unit:
                    summed_ingredients[key]["amount"] += amount

        # Convert the defaultdict to a regular list of dictionaries
        final_ingredients = [
            {"ingredient_type": str(k), "amount": v["amount"], "unit": str(v["unit"])}
            for k, v in summed_ingredients.items()
        ]
        FINAL_INGREDIENTS_GLOBAL = final_ingredients
        return jsonify({"status": "success"})
    else:
        return jsonify({"status": "failure"})


@bp.route("/overview/<user_uuid>", methods=["GET"])
def overview(user_uuid):
    global FINAL_INGREDIENTS_GLOBAL
    final_ingredients = FINAL_INGREDIENTS_GLOBAL
    return render_template(
        "app/shoppinglist.html",
        final_ingredients=final_ingredients,
    )
