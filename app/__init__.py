import os

from flask import Flask
from werkzeug.middleware.proxy_fix import ProxyFix
from flask_login import LoginManager
from app.user import User

# Logging Config
from logging.config import dictConfig

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)


def create_app(test_config=None):
    # create and configure the application
    application = Flask(__name__, instance_relative_config=True)
    application.config.from_mapping(
        SECRET_KEY="dev",
        DATABASE=os.path.join(application.instance_path, "dishcrafter.sqlite"),
        PREFERRED_URL_SCHEME="https",
    )

    login_manager = LoginManager()
    login_manager.login_view = "home.index"  # Specify the login view
    login_manager.init_app(application)

    # User loader callback
    @login_manager.user_loader
    def load_user(user_id):
        # Here, use your logic to load a user by ID
        # For example, if you're using a database:
        return User.get_by_user_id(
            user_id
        )  # Assuming you have a get class method in your User model

    if test_config is None:
        # load the instance config, if it exists, when not testing
        application.config.from_pyfile("config.py", silent=True)
    else:
        # load the test config if passed in
        application.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(application.instance_path)
    except OSError:
        pass

    from . import db

    db.init_app(application)
    with application.app_context():
        db.init_db()

    application.wsgi_app = ProxyFix(application.wsgi_app)

    from app import bp_auth

    application.register_blueprint(bp_auth.bp)

    from app import bp_home

    application.register_blueprint(bp_home.bp)
    application.add_url_rule("/", endpoint="index")

    from app import bp_recipe

    application.register_blueprint(bp_recipe.bp)

    from app import bp_meal

    application.register_blueprint(bp_meal.bp)

    from app import bp_shoppinglist

    application.register_blueprint(bp_shoppinglist.bp)

    return application
