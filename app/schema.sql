-- Users Table
CREATE TABLE IF NOT EXISTS users (
  user_id INTEGER PRIMARY KEY AUTOINCREMENT,
  google_id TEXT NOT NULL,
  username TEXT NOT NULL,
  email TEXT UNIQUE NOT NULL,
  user_uuid TEXT UNIQUE NOT NULL,
  creation_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Recipes Table
CREATE TABLE IF NOT EXISTS recipes (
    recipe_id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    title TEXT NOT NULL,
    description TEXT,
    portion_serves INTEGER,
    note TEXT,
    web_link TEXT,
    image_url TEXT,
    creation_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);

-- Ingredients Table
CREATE TABLE IF NOT EXISTS ingredients (
    ingredient_id INTEGER PRIMARY KEY AUTOINCREMENT,
    unit TEXT NOT NULL,
    ingredient_type TEXT NOT NULL,
    user_id INTEGER NOT NULL
);

-- RecipeIngredients Table (Junction table for Recipes and Ingredients)
CREATE TABLE IF NOT EXISTS recipe_ingredients (
    recipe_id INTEGER NOT NULL,
    ingredient_id INTEGER NOT NULL,
    amount INTEGER NOT NULL,
    PRIMARY KEY (recipe_id, ingredient_id),
    FOREIGN KEY (recipe_id) REFERENCES recipes(recipe_id),
    FOREIGN KEY (ingredient_id) REFERENCES ingredients(ingredient_id)
);

-- SharedRecipes Table
CREATE TABLE IF NOT EXISTS shared_recipes (
    shared_recipe_id INTEGER PRIMARY KEY AUTOINCREMENT,
    recipe_id INTEGER NOT NULL,
    owner_user_id INTEGER NOT NULL,
    shared_with_user_id INTEGER NOT NULL,
    share_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (recipe_id) REFERENCES recipes(recipe_id),
    FOREIGN KEY (owner_user_id) REFERENCES users(user_id),
    FOREIGN KEY (shared_with_user_id) REFERENCES users(user_id)
);

-- MealPlans Table
CREATE TABLE IF NOT EXISTS meal_plans (
    meal_plan_id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    mealplan_data TEXT NOT NULL, -- JSON data storing the whole meal plan
    plan_date DATE NOT NULL, -- The date for which the meal plan is intended
    creation_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);