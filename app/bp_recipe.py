import re
import bleach
from flask import (
    Blueprint,
    render_template,
    request,
    flash,
    session,
    url_for,
    redirect,
    current_app,
    jsonify,
)
from flask_login import login_required
from app.recipe import Recipe
from app.user import User
from html import escape

bp = Blueprint("recipe", __name__, url_prefix="/recipe")


def _get_user_id():
    return session.get("_user_id")


def sanitize_input(input_string):
    # Bleach clean with no allowed tags or attributes (strictest cleaning)
    return bleach.clean(input_string, tags=[], attributes={}, strip=True)


def get_form_parameters(request):
    character_error = False

    # Sanitize inputs using Bleach
    title = sanitize_input(request.form["menuTitle"])
    description = sanitize_input(request.form["menuDescription"])
    note = sanitize_input(request.form["menuNote"])
    prep_link = sanitize_input(request.form["menuPrepLink"])
    image_url = sanitize_input(request.form["menuImageUrl"])
    serves = sanitize_input(request.form["menuServes"])

    ingredients = []
    for key in request.form.keys():
        match = re.match(r"ingredient_type\[(\d+)\]", key)
        if match:
            i = match.group(1)

            ingredient_type = sanitize_input(request.form[f"ingredient_type[{i}]"])
            amount = sanitize_input(request.form[f"ingredient_amount[{i}]"])
            unit = sanitize_input(request.form[f"ingredient_unit[{i}]"])

            try:
                amount = float(amount)
            except ValueError:
                flash("Invalid amount, only use 0-9.", "error")
                character_error = True

            ingredients.append(
                {
                    "ingredient_type": ingredient_type,
                    "amount": amount,
                    "unit": unit,
                }
            )

    recipe = {
        "title": title,
        "description": description,
        "portion_serves": serves,
        "ingredients": ingredients,
        "note": note,
        "web_link": prep_link,
        "image_url": image_url,
        "user_id": _get_user_id(),
    }
    return recipe, character_error


@bp.route("/add", methods=("GET", "POST"))
@login_required
def add():
    if request.method == "POST":
        new_recipe, character_error = get_form_parameters(request=request)
        if character_error:
            return render_template("app/add_recipe.html")

        rec_id = Recipe.create(**new_recipe)
        flash(f"Success, recepie saved with id:{rec_id}!", "success")
        return render_template("app/add_recipe.html")
    else:
        return render_template("app/add_recipe.html")


@bp.route("/edit/<int:recipe_id>", methods=["GET"])
@login_required
def edit(recipe_id):
    recipe = Recipe.get_recipe_by_id(recipe_id=recipe_id)
    return render_template("app/edit_recipe.html", recipe=recipe, recipe_id=recipe_id)


@bp.route("/update/<int:recipe_id>", methods=["POST"])
@login_required
def update(recipe_id):
    try:
        new_recipe, character_error = get_form_parameters(request=request)
        if character_error:
            recepies = Recipe.get_recipes_for_user_id(_get_user_id())
            flash(message="Encountered illegal character, aborting!", category="error")
            current_app.logger.warning(f"Illegal character in Recipe: {request.form}")
            return render_template("app/edit_recipe.html", recepies=recepies)

        # Updated Recipe
        new_recipe["recipe_id"] = recipe_id
        recipe_id = Recipe.update(**new_recipe)
        flash(
            message=f"Successfully updated recipe ID {recipe_id}!", category="success"
        )
        recepies = Recipe.get_recipes_for_user_id(_get_user_id())
        return render_template("app/list_recipes.html", recepies=recepies)
    except Exception as e:
        current_app.logger.error(f"An error occurred: {e}", exc_info=True)
        flash(
            message=f"Something went wrong updating recipe ID: {recipe_id}!",
            category="error",
        )
        recepies = Recipe.get_recipes_for_user_id(_get_user_id())
        return render_template("app/list_recipes.html", recepies=recepies)


@bp.route("/list", methods=["GET"])
@login_required
def list():
    user_id = session.get("_user_id")
    recepies = Recipe.get_recipes_for_user_id(user_id)
    shared_recipes = Recipe.get_shared_recipes(user_id)
    return render_template("app/list_recipes.html", recepies=recepies + shared_recipes)


@bp.route("/share", methods=("GET", "POST"))
@login_required
def share():
    user_id = session.get("_user_id")
    recepies = Recipe.get_recipes_for_user_id(user_id)
    return render_template("app/share_recipes.html", recepies=recepies)


@bp.route("/sharewith", methods=("GET", "POST"))
@login_required
def sharewith():
    data = request.json
    user_id = session.get("_user_id")
    if user_id == int(data["userId"]):
        flash("Can't share recipes with yourself", "error")
        return jsonify({"status": "error"})
    if User.get_by_user_id(data["userId"]):
        for recipe_id in data["recipeIds"]:
            Recipe.share_recipe(
                recipe_id=recipe_id,
                owner_user_id=user_id,
                shared_with_user_id=data["userId"],
            )
    flash("Shared recipes with user, if user exists!", "message")
    return jsonify({"status": "success"})


@bp.route("/delete/<int:recipe_id>", methods=["POST"])
@login_required
def delete_recipe(recipe_id):
    user_id = session.get("_user_id")
    if Recipe.delete(recipe_id=recipe_id, user_id=user_id):
        flash(f"Recipe with ID: {recipe_id} deleted!", "success")
    else:
        flash(f"Error deleting recipe with ID: {recipe_id}!", "error")
    return redirect(url_for("recipe.list"))
