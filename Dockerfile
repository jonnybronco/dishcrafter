# Dishcrafter App
FROM python:3.11

ARG GOOGLE_CLIENT_SECRET
ARG GOOGLE_CLIENT_ID

ENV GOOGLE_CLIENT_ID=$GOOGLE_CLIENT_ID
ENV GOOGLE_CLIENT_SECRET=$GOOGLE_CLIENT_SECRET

ENV TZ=Europe/Zurich
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Setup correct directory structure
ENV PYTHONPATH "${PYTHONPATH}:app/"

# Copy relevant files
COPY ./ /
RUN pip install -r requirements.txt
CMD python app/main.py
