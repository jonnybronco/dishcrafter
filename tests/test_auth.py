import pytest
from app.auth.google_sso import set_google_sso_callback
from app.user import User
from app.db import get_db
from flask_login import login_user
from flask import url_for


# Define a single mock function that can return both true and false responses
def mock_sso_login_process(email_verified):
    class MockSSOResponse:
        def json(self):
            return {
                "email_verified": email_verified,
                "sub": "1234567890" if email_verified else None,
                "given_name": "John" if email_verified else None,
                "email": "john.doe@example.com" if email_verified else None,
            }

    return MockSSOResponse()


@pytest.mark.parametrize(
    "email_verified,expected_result",
    [
        (True, ("1234567890", "John", "john.doe@example.com")),
        (False, (None, None, None)),
    ],
)
def test_google_sso(monkeypatch, email_verified, expected_result):
    # Use monkeypatch to replace _sso_login_process based on the parametrized values
    monkeypatch.setattr(
        "app.auth.google_sso._sso_login_process",
        lambda request: mock_sso_login_process(email_verified),
    )

    class MockRequest:
        args = {"code": "dummycode"}
        url = "http://yourapp.com/auth/google_sso/callback?code=dummycode"
        base_url = "http://yourapp.com/auth/google_sso/callback"

    result = set_google_sso_callback(MockRequest())

    assert result == expected_result


def test_logged_in_user(client):
    with client.application.app_context():
        unique_id = 111
        user_name = "Test"
        user_email = "test@test.com"

        User.create(unique_id, user_name, user_email)
        test_user = (
            get_db()
            .execute("SELECT * FROM users WHERE google_id = ?", (111,))
            .fetchone()
        )
        user = User(
            google_id=unique_id,
            user_name=user_name,
            user_email=user_email,
        )
        with client.application.test_request_context():
            login_user(user)
        response = client.get(url_for("index"))
        assert "Add Recipe" in response.text
        client.get(url_for("auth.logout"))
        response = client.get(url_for("index"))
        assert "Add Recipe" not in response.text


def test_restricted_page(client):
    with client.application.app_context():
        response = client.get(url_for("recipe.add"), follow_redirects=True)
        assert "Welcome" in response.text
