import os
import tempfile

import pytest
from app import create_app
from app.db import get_db, init_db
from app.user import User

with open(os.path.join(os.path.dirname(__file__), "../app/schema.sql"), "rb") as f:
    _data_sql = f.read().decode("utf8")


@pytest.fixture
def app():
    db_fd, db_path = tempfile.mkstemp()

    app = create_app(
        {
            "TESTING": True,
            "DATABASE": db_path,
            "SERVER_NAME": "localhost.localdomain",
            "APPLICATION_ROOT": "/",
        }
    )

    with app.app_context():
        init_db()
        get_db().executescript(_data_sql)

    yield app

    os.close(db_fd)
    os.unlink(db_path)


@pytest.fixture
def client(app):
    return app.test_client()
