import pytest
import json
from app.user import User


def test_get_route(client):
    # Simulate login as this data is fetched for a logged-in user
    with client.session_transaction() as sess:
        sess["_user_id"] = 1  # Assuming 1 is a valid user_id in your test DB

    # Prepare the request data
    request_data = {"titles": ["1", "2"]}

    # Send POST request to /shoppinglist/get
    response = client.post("/shoppinglist/get", json=request_data)

    # Check if the response is successful
    assert response.status_code == 200
    assert response.json == {"status": "success"}


def test_overview_route(client):
    with client.application.app_context():
        user_id = User.create(google_id=1, user_email="a@a.com", user_name="a")
        user = User.get_by_user_id(user_id)
        global FINAL_INGREDIENTS_GLOBAL
        FINAL_INGREDIENTS_GLOBAL = []
        # Make a GET request to the overview route
        response = client.get(f"/shoppinglist/overview/{user.user_uuid}")

        # Check if the response is successful
        assert response.status_code == 200
        # Further checks can be added based on the expected output
