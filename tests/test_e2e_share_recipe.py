import pytest
from werkzeug.datastructures import ImmutableMultiDict
from app.user import User
from app.db import get_db
from flask_login import login_user, logout_user


@pytest.fixture
def imut_dict_response():
    return ImmutableMultiDict(
        [
            ("menuTitle", "Abacus"),
            ("menuDescription", "Test"),
            ("ingredient_amount[0]", "1"),
            ("ingredient_unit[0]", "g"),
            ("ingredient_type[0]", "Salz"),
            ("ingredient_amount[1]", "1"),
            ("ingredient_unit[1]", "g"),
            ("ingredient_type[1]", "Pfeffer"),
            ("menuServes", "2"),
            ("menuNote", "Test"),
            ("menuImageUrl", "http://www.google.com"),
            ("menuPrepLink", "http://www.google.com"),
        ]
    )


@pytest.fixture
def logged_in_user(client):
    with client.application.app_context():
        user_id = User.create(
            google_id=111,
            user_name="Test",
            user_email="a@b.com",
        )
        user = User.get_by_user_id(user_id)
        with client.application.test_request_context():
            login_user(user)
            with client.session_transaction() as sess:
                sess["_user_id"] = 1


def test_share_recipe_user1_with_user2_get_to_shoppinglist(client, imut_dict_response):
    with client.application.app_context():
        first_user_id = User.create(
            google_id=111,
            user_name="Test",
            user_email="a@b.com",
        )
        second_user_id = User.create(
            google_id=112,
            user_name="Test",
            user_email="a@c.com",
        )
        user1 = User.get_by_user_id(first_user_id)
        user2 = User.get_by_user_id(second_user_id)
        with client.application.test_request_context():
            login_user(user1)
            with client.session_transaction() as sess:
                sess["_user_id"] = 1
        response = client.post("recipe/add", data=imut_dict_response)
        assert "Success" in response.text
        response = client.get("recipe/share")
        assert "<p hidden>1</p>" in response.text
        payload = {"recipeIds": ["1"], "userId": "1"}
        headers = {"Content-Type": "application/json"}
        # Do not share with yourself
        response = client.post("/recipe/sharewith", json=payload, headers=headers)
        assert '"status":"error"' in response.text
        # Share with user 2
        payload = {"recipeIds": ["1"], "userId": "2"}
        response = client.post("/recipe/sharewith", json=payload, headers=headers)
        assert '"status":"success"' in response.text
        with client.application.test_request_context():
            logout_user()
            login_user(user2)
            with client.session_transaction() as sess:
                sess["_user_id"] = 2
        response = client.get("recipe/list")
        # Assert testing user 2
        assert "User-ID: 2" in response.text
        # Assert title of shared recipe is visible
        assert "Abacus" in response.text
        response = client.get("mealplan/")
        # Ensure recipe is visible in mealplan overview
        assert 'recipe-id="1"' in response.text
        payload = {"titles": ["1"]}
        headers = {"Content-Type": "application/json"}
        response = client.post(f"shoppinglist/get", json=payload, headers=headers)
        assert '"status":"success"' in response.text
        response = client.get(f"shoppinglist/overview/{user2.user_uuid}")
        # Ensure Peppers is listed as ingredient
        assert "Pfeffer, 1" in response.text
