import pytest
from werkzeug.datastructures import ImmutableMultiDict
from app.recipe import Recipe
from app.user import User
from app.db import get_db
from flask_login import login_user
from flask import session


@pytest.fixture
def basic_recipe():
    return {
        "user_id": 1,
        "title": "A Test Menu",
        "description": "Some Description",
        "portion_serves": 1,
        "ingredients": [
            {"ingredient_type": "Salt", "amount": 1.0, "unit": "g"},
            {"ingredient_type": "Pepper", "amount": 1.0, "unit": "g"},
            {"ingredient_type": "Carrot", "amount": 5.0, "unit": "Piece"},
        ],
        "note": "LoremIpsum",
        "web_link": "https://google.com",
        "image_url": "https://google.com",
    }


def test_delete_recipe(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        Recipe.delete(recipe_id, user_id=1)
        recipe = Recipe.get_recipe_by_id(recipe_id)
        assert recipe is None


def test_create_recipe(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        part_recpie = Recipe.get_recipe_by_id(recipe_id=recipe_id)
        assert part_recpie["title"] == "A Test Menu"


def test_save_user_recepie_with_same_ingredients(client, basic_recipe):
    with client.application.app_context():
        Recipe.create(**basic_recipe)
        Recipe.create(**basic_recipe)
        db = get_db()
        rows = db.execute(
            "SELECT ingredient_id FROM ingredients WHERE ingredient_type = ?",
            ("Salt",),
        ).fetchall()
        assert len(rows) == 1
        rows = db.execute(
            """
            SELECT r.recipe_id, r.user_id, r.title, r.description, r.portion_serves, r.note, r.web_link, 
                i.ingredient_type, ri.amount, i.unit
            FROM recipes r
            JOIN recipe_ingredients ri ON r.recipe_id = ri.recipe_id
            JOIN ingredients i ON ri.ingredient_id = i.ingredient_id
            WHERE r.user_id = ?
            ORDER BY r.recipe_id
            """,
            (1,),
        ).fetchall()
        recipes = {}
        for row in rows:
            recipe_id = row[0]  # Correctly use the recipe_id as the unique identifier

            if recipe_id not in recipes:
                recipes[recipe_id] = {
                    "user_id": row[1],
                    "title": row[2],
                    "description": row[3],
                    "portion_serves": row[4],
                    "note": row[5],
                    "web_link": row[6],
                    "ingredients": [],
                }

            ingredient = {"ingredient_type": row[7], "amount": row[8], "unit": row[9]}
            recipes[recipe_id]["ingredients"].append(ingredient)
        recipes = list(recipes.values())
        assert len(recipes[0]["ingredients"]) == 3


def test_update_basic_recipe_parameters(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        basic_recipe["recipe_id"] = recipe_id
        basic_recipe["title"] = "Hello Update"
        basic_recipe["description"] = "Hello Update"
        basic_recipe["portion_serves"] = "Hello Update"
        basic_recipe["note"] = "Hello Update"
        basic_recipe["web_link"] = "Hello Update"
        recipe_id = Recipe.update(**basic_recipe)
        recipe = Recipe.get_recipe_by_id(recipe_id=recipe_id)
        assert recipe["title"] == "Hello Update"
        assert recipe["description"] == "Hello Update"
        assert recipe["portion_serves"] == "Hello Update"
        assert recipe["note"] == "Hello Update"
        assert recipe["web_link"] == "Hello Update"


def test_update_receipe_with_new_ingredient(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        basic_recipe["recipe_id"] = recipe_id
        basic_recipe["ingredients"].append(
            {"ingredient_type": "Kurkuma", "amount": 10.0, "unit": "g"},
        )
        recipe_id = Recipe.update(**basic_recipe)
        recipe = Recipe.get_recipe_by_id(recipe_id=recipe_id)
        assert len(recipe["ingredients"]) == 4, "The list should now be one item longer"
        contains_kurkuma = any(
            ingredient["ingredient_type"] == "Kurkuma"
            for ingredient in basic_recipe["ingredients"]
        )
        assert contains_kurkuma, "'Kurkuma should be in the ingredients list!'"


def test_update_recipe_with_ingredient_amount(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        basic_recipe["recipe_id"] = recipe_id
        basic_recipe["ingredients"].append(
            {"ingredient_type": "Salt", "amount": 2.0, "unit": "g"},
        )  # Update the amount from 1.0 to to 2.0
        recipe_id = Recipe.update(**basic_recipe)
        recipe = Recipe.get_recipe_by_id(recipe_id=recipe_id)
        assert recipe["ingredients"][0]["amount"] == 2.0


def test_update_recipe_with_deleted_ingredient(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        basic_recipe["recipe_id"] = recipe_id
        basic_recipe["ingredients"] = [
            {"ingredient_type": "Pepper", "amount": 1.0, "unit": "g"},
            {"ingredient_type": "Carrot", "amount": 5.0, "unit": "Piece"},
        ]
        recipe_id = Recipe.update(**basic_recipe)
        recipe = Recipe.get_recipe_by_id(recipe_id=recipe_id)
        assert len(recipe["ingredients"]) == 2


def test_share_recipe_with_one_user(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        owner_user_id = 1
        shared_with_user_id = 2
        Recipe.share_recipe(recipe_id, owner_user_id, shared_with_user_id)
        db = get_db()
        cursor = db.cursor()
        cursor.execute("SELECT * FROM shared_recipes WHERE recipe_id = ?", (recipe_id,))
        shared_recipe = cursor.fetchone()
        assert shared_recipe is not None
        assert shared_recipe["owner_user_id"] == owner_user_id
        assert shared_recipe["shared_with_user_id"] == shared_with_user_id


def test_share_recipe_twice_with_one_user(client, basic_recipe):
    with client.application.app_context():
        recipe_id = Recipe.create(**basic_recipe)
        owner_user_id = 1
        shared_with_user_id = 2
        # Share recipe twice
        Recipe.share_recipe(recipe_id, owner_user_id, shared_with_user_id)
        Recipe.share_recipe(recipe_id, owner_user_id, shared_with_user_id)
        db = get_db()
        cursor = db.cursor()
        cursor.execute("SELECT * FROM shared_recipes WHERE recipe_id = ?", (recipe_id,))
        shared_recipes = cursor.fetchall()
        assert len(shared_recipes) == 1
        shared_recipe = shared_recipes[0]
        assert shared_recipe["owner_user_id"] == owner_user_id
        assert shared_recipe["shared_with_user_id"] == shared_with_user_id


def test_get_shared_recipes(client, basic_recipe):
    with client.application.app_context():
        # Prepare database with users
        User.create(1, "a", "a@a.com")
        User.create(2, "b", "b@b.com")

        owner_user_id = 1
        shared_with_user_id = 2
        recipe_id = Recipe.create(**basic_recipe)
        Recipe.share_recipe(recipe_id, owner_user_id, shared_with_user_id)
        shared_recipes = Recipe.get_shared_recipes(2)
        assert len(shared_recipes) == 1
