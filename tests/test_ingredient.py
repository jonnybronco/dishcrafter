import pytest
from app.ingredient import Ingredient


@pytest.fixture
def standard_ingredient(client):
    with client.application.app_context():
        return Ingredient(unit="g", ingredient_type="Salt")


def test_ingredient_create_get_by_ingredient_type(client, standard_ingredient):
    with client.application.app_context():
        ingredient_id = Ingredient.create(
            unit=standard_ingredient.unit,
            ingredient_type=standard_ingredient.ingredient_type,
            user_id=1,
        )
        ingredient = Ingredient.get_by_id(ingredient_id)
        assert "Salt" in ingredient[2]


def test_create_delete_ingredient(client, standard_ingredient):
    with client.application.app_context():
        ingredient_id = Ingredient.create(
            unit=standard_ingredient.unit,
            ingredient_type=standard_ingredient.ingredient_type,
            user_id=1,
        )
        ingredient = Ingredient.get_by_id(ingredient_id)
        Ingredient.delete_by_id(ingredient[0])
        assert Ingredient.get_by_id(ingredient[0]) is None


def test_get_ingredient_by_type(client, standard_ingredient):
    with client.application.app_context():
        ingredient_id = Ingredient.create(
            unit=standard_ingredient.unit,
            ingredient_type=standard_ingredient.ingredient_type,
            user_id=1,
        )
        ingredient = Ingredient.get_by_id(ingredient_id)
        types = Ingredient.get_by_type(ingredient[2])
        assert len(types) == 1


def test_create_update_ingredient(client, standard_ingredient):
    with client.application.app_context():
        ingredient_id = Ingredient.create(
            unit=standard_ingredient.unit,
            ingredient_type=standard_ingredient.ingredient_type,
            user_id=1,
        )
        ingredient = Ingredient.get_by_id(ingredient_id)
        Ingredient.update(id=ingredient_id, unit="g", ingredient_type="Pepper")
        ingredient = Ingredient.get_by_id(ingredient_id)
        assert ingredient[2] == "Pepper"
