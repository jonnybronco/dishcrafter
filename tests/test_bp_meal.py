def test_bp_meal_overview(
    client,
):
    with client.application.app_context():
        response = client.get("mealplan/")
        assert response.status_code == 200
