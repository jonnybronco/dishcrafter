import sqlite3

import pytest
from app.db import get_db, init_db_command
import click
from click.testing import CliRunner


def test_get_close_db(app):
    with app.app_context():
        db = get_db()
        assert db is get_db()

    with pytest.raises(sqlite3.ProgrammingError) as e:
        db.execute("SELECT 1")

    assert "closed" in str(e.value)


@pytest.fixture
def runner():
    return CliRunner()


def test_init_db_command(runner, client):
    with client.application.app_context():
        # Invoke the init-db command
        result = runner.invoke(init_db_command)

        # Check that the command was successful and the output is correct
        assert "Initialized the database." in result.output
        assert result.exit_code == 0
