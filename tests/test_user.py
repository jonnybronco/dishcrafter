import pytest
from app.user import User


@pytest.fixture
def user_object():
    a_user = User(id=1, google_id=1212, user_name="Test", user_email="test@test.com")
    return a_user


def test_user_creation(client, user_object):
    with client.application.app_context():
        User.create(
            google_id=user_object.google_id,
            user_name=user_object.user_name,
            user_email=user_object.user_email,
        )
        assert User.get_by_google_id(1212) is not None
        test_user = User.get_by_google_id(1212)
        assert test_user.user_email == "test@test.com"
        assert test_user.user_name == "Test"


def test_no_user(client):
    with client.application.app_context():
        test_user = User.get_by_user_id(1212)
        assert test_user == None
        test_user = User.get_by_google_id(1212)
        assert test_user == None
