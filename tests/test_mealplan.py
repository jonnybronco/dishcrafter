import pytest
from datetime import datetime
from app.mealplan import MealPlan
from app.db import get_db
import json


@pytest.fixture
def basic_mealplan():
    return {
        "menuItems": [
            {"recipeId": "33", "day": "Thursday", "meal": "lunch"},
            {"recipeId": "53", "day": "Thursday", "meal": "dinner"},
            {"recipeId": "55", "day": "Friday", "meal": "lunch"},
            {"recipeId": "41", "day": "Friday", "meal": "dinner"},
            {"recipeId": "42", "day": "Saturday", "meal": "lunch"},
            {"recipeId": "36", "day": "Saturday", "meal": "dinner"},
            {"recipeId": "15", "day": "Sunday", "meal": "lunch"},
            {"recipeId": "2", "day": "Sunday", "meal": "dinner"},
            {"recipeId": "54", "day": "Monday", "meal": "lunch"},
            {"recipeId": "59", "day": "Monday", "meal": "dinner"},
            {"recipeId": "51", "day": "Tuesday", "meal": "lunch"},
            {"recipeId": "39", "day": "Tuesday", "meal": "dinner"},
            {"recipeId": "18", "day": "Wednesday", "meal": "lunch"},
            {"recipeId": "37", "day": "Wednesday", "meal": "dinner"},
        ]
    }


def test_save_current_mealplan(client, basic_mealplan):
    with client.application.app_context():
        user_id = 1
        MealPlan.save_current_mealplan(user_id=user_id, mealplan_data=basic_mealplan)
        saved_plan = MealPlan.get_last_mealplan(user_id=user_id)
        assert saved_plan is not None
        assert saved_plan["menuItems"] == basic_mealplan["menuItems"]


def test_get_last_mealplan(client, basic_mealplan):
    with client.application.app_context():
        user_id = 1
        MealPlan.save_current_mealplan(user_id=user_id, mealplan_data=basic_mealplan)
        last_mealplan = MealPlan.get_last_mealplan(user_id=user_id)
        assert last_mealplan is not None
        assert last_mealplan == basic_mealplan


def test_get_last_mealplan_when_none_exist(client):
    with client.application.app_context():
        user_id = 1
        last_mealplan = MealPlan.get_last_mealplan(user_id=user_id)
        assert last_mealplan is None


def test_save_empty_mealplan(client):
    with client.application.app_context():
        user_id = 1
        empty_mealplan = {"menuItems": []}
        MealPlan.save_current_mealplan(user_id=user_id, mealplan_data=empty_mealplan)
        db = get_db()
        saved_plan = MealPlan.get_last_mealplan(user_id=user_id)
        assert saved_plan is not None
        assert saved_plan["menuItems"] == empty_mealplan["menuItems"]


def test_get_last_mealplan_for_different_users(client, basic_mealplan):
    with client.application.app_context():
        user_id_1 = 1
        user_id_2 = 2

        # Save meal plan for the first user
        MealPlan.save_current_mealplan(user_id=user_id_1, mealplan_data=basic_mealplan)

        # Save a different meal plan for the second user
        another_mealplan = {
            "menuItems": [
                {
                    "day_of_week": "Wednesday",
                    "meal_type": "lunch",
                    "recipe_id": 5,
                    "plan_date": "2024-08-28",
                },
                {
                    "day_of_week": "Wednesday",
                    "meal_type": "dinner",
                    "recipe_id": 6,
                    "plan_date": "2024-08-28",
                },
            ]
        }
        MealPlan.save_current_mealplan(
            user_id=user_id_2, mealplan_data=another_mealplan
        )

        # Retrieve and test the meal plan for the first user
        last_mealplan_user_1 = MealPlan.get_last_mealplan(user_id=user_id_1)
        assert last_mealplan_user_1 == basic_mealplan

        # Retrieve and test the meal plan for the second user
        last_mealplan_user_2 = MealPlan.get_last_mealplan(user_id=user_id_2)
        assert last_mealplan_user_2 == another_mealplan
