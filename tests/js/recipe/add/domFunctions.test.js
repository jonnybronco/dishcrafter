import {
  validateInput,
  addIngredientRow,
  deleteRow
} from '../../../../app/static/js/recipe/addRecipe/domFunctions.js';

describe('validateInput', () => {
  // Mock the alert to prevent it from showing up during tests
  window.alert = jest.fn();

  // Mock focus method
  HTMLInputElement.prototype.focus = jest.fn();

  it('returns true for valid input', () => {
    // Mock an input element with a valid value
    const input = document.createElement('input');
    input.value = 'Valid Input 123';

    // Call the function with the mocked input
    const result = validateInput(input);

    // Assertions
    expect(result).toBe(true);
    expect(window.alert).not.toHaveBeenCalled();
    expect(input.focus).not.toHaveBeenCalled();
  });

  it('returns true for empty input', () => {
    // Mock an input element with an empty value
    const input = document.createElement('input');
    input.value = '';

    // Call the function with the mocked input
    const result = validateInput(input);

    // Assertions
    expect(result).toBe(true);
    expect(window.alert).not.toHaveBeenCalled();
    expect(input.focus).not.toHaveBeenCalled();
  });

  it('returns false and shows alert for invalid input', () => {
    // Mock an input element with an invalid value
    const input = document.createElement('input');
    input.value = 'Invalid#Input';

    // Call the function with the mocked input
    const result = validateInput(input);

    // Assertions
    expect(result).toBe(false);
    expect(window.alert).toHaveBeenCalledWith('Invalid input: ' + input.value);
    expect(input.focus).toHaveBeenCalled();
  });
});

beforeEach(() => {
  document.body.innerHTML = `
      <div id="ingredientFields"></div>
    `;
  // Reset ingredientIndex to 1 before each test to ensure consistent starting conditions
  const {
    resetIngredientIndex
  } = require('../../../../app/static/js/recipe/addRecipe/domFunctions.js');
  resetIngredientIndex();
});

describe('Ingredient Rows', () => {
  it('adds a new ingredient row correctly', () => {
    // Initially, there should be no rows
    expect(document.querySelectorAll('.ingredientContainer').length).toBe(0);

    // Add a row
    addIngredientRow();

    // Now, there should be one row
    let rows = document.querySelectorAll('.ingredientContainer');
    expect(rows.length).toBe(1);
    expect(rows[0].id).toBe('ingredientRow1');

    // Add another row
    addIngredientRow();

    // Check for two rows
    rows = document.querySelectorAll('.ingredientContainer');
    expect(rows.length).toBe(2);
    expect(rows[1].id).toBe('ingredientRow2');
  });
});

describe('Delete Ingredient Row', () => {
  it('deletes a specific ingredient row and re-indexes subsequent rows', () => {
    // Add three rows for testing
    addIngredientRow();
    addIngredientRow();
    addIngredientRow();
    let test = document.querySelectorAll('.ingredientContainer');

    expect(document.querySelectorAll('.ingredientContainer').length).toBe(3);

    // Delete the second row
    deleteRow(3);

    const rows = document.querySelectorAll('.ingredientContainer');
    expect(rows.length).toBe(2);
    expect(rows[0].id).toBe('ingredientRow1');
    expect(rows[1].id).toBe('ingredientRow2'); // Note: This was ingredientRow3 before deletion

    // Ensure the names of inputs in the last row have been updated
    expect(
      rows[1].querySelector('input[name="ingredient_amount[2]"]')
    ).not.toBeNull();
    expect(
      rows[1].querySelector('input[name="ingredient_unit[2]"]')
    ).not.toBeNull();
    expect(
      rows[1].querySelector('input[name="ingredient_type[2]"]')
    ).not.toBeNull();

    // Add more rows and then delete the first one
    addIngredientRow();
    addIngredientRow();
    addIngredientRow();

    deleteRow(1);
    // Test if re-indexing has worked
    const rows_amount = document.querySelectorAll('.ingredientContainer');
    expect(rows_amount[0].id).toBe('ingredientRow1');
  });
});

describe('Delete Ingredient Row - Non-existent Row', () => {
  it('does nothing if the row does not exist', () => {
    // Setup - Ensure there is at least one row for context
    addIngredientRow();

    // Initial count of rows
    const initialRowCount = document.querySelectorAll(
      '.ingredientContainer'
    ).length;

    // Try to delete a non-existent row, e.g., with an index that is too high
    deleteRow(999);

    // Final count of rows after attempting to delete
    const finalRowCount = document.querySelectorAll(
      '.ingredientContainer'
    ).length;

    // Check that the row count remains unchanged
    expect(finalRowCount).toBe(initialRowCount);
  });
});
