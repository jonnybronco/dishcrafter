// Import the functions to test
import {
  incrementWeekOffset,
  decrementWeekOffset,
  getWeekOffset
} from '../../../app/static/js/mealplan/stateManager.js';

describe('weekOffsetManager', () => {
  // Before each test, reset weekOffset to its initial state
  beforeEach(() => {
    // Resetting weekOffset by setting it to 0 before each test
    // Since weekOffset is not directly accessible, use getWeekOffset to check its value
    // and decrementWeekOffset or incrementWeekOffset to adjust it to 0
    const currentOffset = getWeekOffset();
    if (currentOffset > 0) {
      Array.from({ length: currentOffset }).forEach(() =>
        decrementWeekOffset()
      );
    } else {
      Array.from({ length: -currentOffset }).forEach(() =>
        incrementWeekOffset()
      );
    }
  });

  test('initial weekOffset is 0', () => {
    expect(getWeekOffset()).toBe(0);
  });

  test('incrementWeekOffset increases weekOffset by 1', () => {
    incrementWeekOffset();
    expect(getWeekOffset()).toBe(1);
  });

  test('decrementWeekOffset decreases weekOffset by 1', () => {
    // First, increment to ensure weekOffset is not at its initial value
    incrementWeekOffset();
    // Then, decrement back
    decrementWeekOffset();
    expect(getWeekOffset()).toBe(0);
  });

  test('multiple increments and decrements produce the correct weekOffset', () => {
    incrementWeekOffset();
    incrementWeekOffset();
    decrementWeekOffset();
    incrementWeekOffset();
    expect(getWeekOffset()).toBe(2);
  });
});
