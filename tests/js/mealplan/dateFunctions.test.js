const {
  getWeekNumber,
  getWeekDates
} = require('../../../app/static/js/mealplan/dateFunctions.js');

describe('Date Functions', () => {
  describe('getWeekNumber', () => {
    test('returns correct week number for a given date', () => {
      // Example date: January 1, 2024
      const date = new Date(2024, 0, 1); // Remember, months are 0-indexed in JavaScript Date
      expect(getWeekNumber(date)).toBe(1);
    });
  });

  describe('getWeekDates', () => {
    test('returns correct week dates for a 0 week offset', () => {
      const now = new Date(2024, 0, 1); // Assuming this date is a Monday for simplicity
      jest.useFakeTimers().setSystemTime(now);

      const weekDates = getWeekDates(0);
      expect(weekDates.Monday.getDate()).toBe(1);
      expect(weekDates.Tuesday.getDate()).toBe(2);
      expect(weekDates.Wednesday.getDate()).toBe(3);
      expect(weekDates.Thursday.getDate()).toBe(4);
      expect(weekDates.Friday.getDate()).toBe(5);
      expect(weekDates.Saturday.getDate()).toBe(6);
      expect(weekDates.Sunday.getDate()).toBe(7);

      jest.useRealTimers();
    });
    test('handles Sunday correctly for week number calculation', () => {
      // Use a date known to be a Sunday
      const sunday = new Date(Date.UTC(2024, 0, 8)); // 7th January 2024 is a Sunday
      expect(getWeekNumber(sunday)).toBe(2);
    });
  });
});
