// First, import the dependencies as modules to mock them.
import { createNoRecipeWrapper } from '../../../app/static/js/mealplan/miscFunctions.js';
import { drop } from '../../../app/static/js/mealplan/ddFunctions.js';

// Directly mock the external module functions used by your function under test.
jest.mock('../../../app/static/js/mealplan/miscFunctions.js', () => ({
  createNoRecipeWrapper: jest.fn()
}));

describe('drop', () => {
  beforeEach(() => {
    // Reset mocks before each test to ensure a clean slate
    createNoRecipeWrapper.mockReset();

    // Setup the DOM structure required for the test
    document.body.innerHTML = `
    <div class="week">
    <div class="day" style="position: relative">
        <h5>
          Monday
          </h5><h6 id="Monday-date">15. April</h6>
        
        <button class="btn move-to-remaining" style="position: absolute; top: 0; right: 0" data-day="Monday">
          Clear
        </button>
        <hr>
        <div class="meal">
          <h6>Lunch</h6>
          <div id="Monday-lunch-drop-zone" class="meal-item drop-target" style="background-image: url('https://google.com');">
            
            <div class="draggable" draggable="true" id="Monday-lunch-text" data-day="Monday" data-meal="lunch" recipe-id="14">
              Menu 1
            </div>
            
          </div>
        </div>
        <p class="break"></p>
        <div class="meal">
          <h6>Dinner</h6>
          <div id="Monday-dinner-drop-zone" class="meal-item drop-target" style="background-image: url('https://bing.com');">
            
            <div class="draggable" draggable="true" id="Monday-dinner-text" data-day="Monday" data-meal="dinner" recipe-id="30">
              Menu 2
            </div>
            
          </div>
        </div>
      </div>
    <div id="remaining-menus-title" class="drop-target">Remaining Menus</div>
    <div id="remaining-menus">
      <div id="remaining-menus-drop-zone" class="meal-item drop-target" style="background-image: url(''); background-size: cover; background-position: center;">
        <div class="draggable" draggable="true" id="recipe_id_1" recipe-id="1">
          Example Recipe Title
        </div>
      </div>
    </div>
    `;

    // Provide mock implementations that use `document` for those functions that manipulate the DOM
    createNoRecipeWrapper.mockImplementation(() => {
      const noRecipeElem = document.createElement('div');
      noRecipeElem.classList.add('draggable');
      noRecipeElem.setAttribute('draggable', 'true');
      noRecipeElem.setAttribute('ondragstart', 'drag(event)');
      // Assuming these attributes are set based on the dragged element in your tests
      noRecipeElem.setAttribute('data-day', 'Monday');
      noRecipeElem.setAttribute('data-meal', 'Lunch');
      noRecipeElem.innerText = 'Nix geplant!';
      // Mock the setDraggable function as well if needed

      const wrapperDiv = document.createElement('div');
      wrapperDiv.classList.add('meal-item', 'drop-target');
      wrapperDiv.appendChild(noRecipeElem);

      return wrapperDiv;
    });
  });

  it('calls preventDefault on the event', () => {
    const dataTransfer = {
      getData: jest.fn().mockReturnValue('Monday-lunch-text'),
      setData: jest.fn()
    };

    const event = {
      preventDefault: jest.fn(),
      dataTransfer,
      target: document.querySelector('.meal-item')
    };

    drop(event);

    expect(event.preventDefault).toHaveBeenCalled();
  });

  it("creates a placeholder and updates text correctly when dropped on 'remaining-menus'", () => {
    // Define the event object here, within the test scope
    const dataTransfer = {
      getData: jest.fn().mockReturnValue('Monday-lunch-text'),
      setData: jest.fn()
    };
    const event = {
      preventDefault: jest.fn(),
      dataTransfer,
      target: document.getElementById('remaining-menus-title')
    };

    // Get amount of childs before drop
    const remainingMenus = document.getElementById('remaining-menus');
    const numberOfChildren = remainingMenus.childElementCount;

    // Call drop function with the event
    drop(event);

    // Now perform the assertions
    const wrapperDiv = document.querySelector('.meal-item.drop-target');
    expect(wrapperDiv).not.toBeNull(); // Ensure the wrapper div was created

    const noRecipeElem = wrapperDiv.querySelector('.draggable');
    expect(noRecipeElem).not.toBeNull(); // Ensure the 'No recipe' element exists
    expect(noRecipeElem.innerText).toBe('Nix geplant!'); // Check the innerText
    expect(noRecipeElem.parentNode.style.backgroundImage).toBe('none'); // Ensure no background image
    expect(noRecipeElem).not.toBeNull(); // Ensure the 'No recipe' element exists

    // Ensure the moved element is in the remaining menus
    const after_remainingMenus = document.getElementById('remaining-menus');
    const after_numberOfChildern = after_remainingMenus.childElementCount;
    expect(numberOfChildren).toBe(1);
    expect(after_numberOfChildern).toBe(2);
  });

  it("swaps text content correctly when dropped on another '.draggable' element", () => {
    const dataTransfer = {
      getData: jest.fn().mockReturnValue('Monday-lunch-text'),
      setData: jest.fn()
    };
    const targetElem = document.getElementById('Monday-dinner-drop-zone');
    const event = {
      preventDefault: jest.fn(),
      dataTransfer,
      target: document.getElementById('Monday-dinner-drop-zone')
    };

    // Call drop function with the event
    drop(event);

    // Perform the assertions
    const dinner = document.getElementById('Monday-dinner-text');
    expect(dinner.innerHTML).toContain('Menu 1');
    expect(dinner.parentElement.style.backgroundImage).toBe(
      'url(https://google.com)'
    );
    const lunch = document.getElementById('Monday-lunch-text');
    expect(lunch.innerHTML).toContain('Menu 2');
    expect(lunch.parentElement.style.backgroundImage).toBe(
      'url(https://bing.com)'
    );
  });

  it('updates background color for draggable elements correctly', () => {
    const dataTransfer = {
      getData: jest.fn().mockReturnValue('Monday-lunch-text'),
      setData: jest.fn()
    };
    const event = {
      preventDefault: jest.fn(),
      dataTransfer,
      target: document.getElementById('remaining-menus-title')
    };

    // Call drop function with the event
    drop(event);

    // Now perform the assertion
    const lunch = document.getElementById('Monday-lunch-text');
    expect(lunch.style.backgroundColor).toBe('rgb(100, 116, 139)');
  });

  test('removes "drag-over" class from target on drop', () => {
    // Setup minimal DOM with a drop target having "drag-over" class
    document.body.innerHTML = `
      <div id="drop-zone" class="meal-item drop-target drag-over">
        <div id="draggable-element" class="draggable">Draggable Content</div>
      </div>
    `;

    // Mock the dataTransfer and event objects
    const dataTransferMock = {
      getData: jest.fn().mockReturnValue('draggable-element')
    };

    const eventMock = {
      preventDefault: jest.fn(),
      dataTransfer: dataTransferMock,
      target: document.querySelector('#drop-zone')
    };

    // Execute the drop function with the mocked event
    drop(eventMock);

    // Assert "drag-over" class is removed from the drop target
    const dropZone = document.getElementById('drop-zone');
    expect(dropZone.classList.contains('drag-over')).toBeFalsy();
  });
});
