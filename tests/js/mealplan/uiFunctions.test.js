import {
  removeHighlight,
  addHighlight
} from '../../../app/static/js/mealplan/uiFunctions.js';

describe('addHighlight', () => {
  test('adds "drag-over" class to closest ".meal-item" parent', () => {
    document.body.innerHTML = `
        <div class="meal-item">
          <div class="child-element"></div>
        </div>
      `;

    const childElement = document.querySelector('.child-element');
    const event = {
      target: childElement
    };

    addHighlight(event);

    const mealItem = document.querySelector('.meal-item');
    expect(mealItem.classList.contains('bg-slate-400')).toBeTruthy();
  });
  test('does not add "drag-over" class when no ".meal-item" parent is found', () => {
    document.body.innerHTML = `
      <div class="not-meal-item">
        <div class="child-element"></div>
      </div>
    `;

    const childElement = document.querySelector('.child-element');
    const event = {
      target: childElement
    };

    addHighlight(event);

    // Verify that no element has been given the "drag-over" class
    const mealItem = document.querySelector('.meal-item');
    expect(mealItem).toBeNull();

    const notMealItem = document.querySelector('.not-meal-item');
    expect(notMealItem.classList.contains('drag-over')).toBeFalsy();
  });
});

describe('removeHighlight', () => {
  test('removes "drag-over" class from ".meal-item" when not dragging over a child element', () => {
    document.body.innerHTML = `
      <div class="meal-item drag-over">
        <div class="child-element"></div>
      </div>
    `;

    const mealItem = document.querySelector('.meal-item');
    // Simulate dragging out of the .meal-item but not over a child element
    const event = {
      target: mealItem,
      relatedTarget: null
    };

    removeHighlight(event);

    expect(mealItem.classList.contains('bg-slate-400')).toBeFalsy();
  });

  test('does not remove "drag-over" class when dragging over a child element of ".meal-item"', () => {
    document.body.innerHTML = `
      <div class="meal-item drag-over">
        <div class="child-element"></div>
      </div>
    `;

    const mealItem = document.querySelector('.meal-item');
    const childElement = document.querySelector('.child-element');
    // Simulate dragging over the child element of the .meal-item
    const event = {
      target: mealItem,
      relatedTarget: childElement
    };

    removeHighlight(event);

    expect(mealItem.classList.contains('drag-over')).toBeTruthy();
  });
});
