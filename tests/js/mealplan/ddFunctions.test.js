import {
  drag,
  allowDrop
} from '../../../app/static/js/mealplan/ddFunctions.js';
import { setDraggable } from '../../../app/static/js/mealplan/uiFunctions.js';

describe('drag', () => {
  // Mocking inside beforeEach to ensure `document` is accessible
  beforeEach(() => {
    jest.mock('../../../app/static/js/mealplan/ddFunctions.js', () => ({
      createNoRecipeWrapper: jest.fn().mockImplementation(() => {
        // Now, this part has access to `document` because it's called within the test runtime
        const div = mockDocument.createElement('div');
        div.className = 'draggable';
        return div;
      })
    }));
  });

  it("sets the correct data on the event's dataTransfer object", () => {
    const event = {
      dataTransfer: {
        setData: jest.fn()
      },
      target: {
        id: 'test-element'
      }
    };

    drag(event);

    expect(event.dataTransfer.setData).toHaveBeenCalledWith(
      'text',
      'test-element'
    );
  });
});

describe('allowDrop', () => {
  it('calls preventDefault on the event', () => {
    // Mock the event with a preventDefault method
    const event = {
      preventDefault: jest.fn()
    };

    // Call the allowDrop function with the mocked event
    allowDrop(event);

    // Assert that preventDefault was called
    expect(event.preventDefault).toHaveBeenCalled();
  });
});

describe('setDraggable', () => {
  it('sets the correct attributes to make an element draggable', () => {
    // Create a mock element
    const element = document.createElement('div');

    // Call setDraggable on the mock element
    setDraggable(element);

    // Assert that the draggable attribute is set to "true"
    expect(element.getAttribute('draggable')).toBe('true');

    // Assert that the ondragstart attribute is correctly set
    expect(element.getAttribute('ondragstart')).toBe('drag(event)');
  });
});
