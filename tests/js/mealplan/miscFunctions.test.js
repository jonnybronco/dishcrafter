// Import the function from your miscFunctions module
import {
  generateUUID,
  createNoRecipeWrapper
} from '../../../app/static/js/mealplan/miscFunctions.js';

describe('createNoRecipeWrapper', () => {
  test('creates wrapper with correct structure and classes', () => {
    // Mock a dragged element with necessary attributes for the function
    const draggedElem = document.createElement('div');
    draggedElem.setAttribute('data-day', 'Monday');
    draggedElem.setAttribute('data-meal', 'Lunch');

    const wrapper = createNoRecipeWrapper(draggedElem);

    // Check if the wrapper and its child have the correct classes
    expect(wrapper.classList.contains('meal-item')).toBeTruthy();
    expect(wrapper.classList.contains('drop-target')).toBeTruthy();

    // Check if the wrapper and its child have the correct attributes
    expect(wrapper.firstChild.getAttribute('data-day')).toBe(null);
    expect(wrapper.firstChild.getAttribute('data-meal')).toBe(null);

    // Check the inner text of the 'No recipe' element
    const children = wrapper.children;
    const secondChild = children[1];
    expect(secondChild.innerText).toBe('Nix geplant!');
  });
});
