import pytest
from werkzeug.datastructures import ImmutableMultiDict
from app.recipe import Recipe
from app.user import User
from app.db import get_db
from flask_login import login_user


@pytest.fixture
def imut_dict_response():
    return ImmutableMultiDict(
        [
            ("menuTitle", "A"),
            ("menuDescription", "Test"),
            ("ingredient_amount[0]", "1"),
            ("ingredient_unit[0]", "g"),
            ("ingredient_type[0]", "Salz"),
            ("ingredient_amount[1]", "1"),
            ("ingredient_unit[1]", "g"),
            ("ingredient_type[1]", "Pfeffer"),
            ("menuServes", "2"),
            ("menuNote", "Test"),
            ("menuImageUrl", "http://www.google.com"),
            ("menuPrepLink", "http://www.google.com"),
        ]
    )


@pytest.fixture
def logged_in_user(client):
    with client.application.app_context():
        user_id = User.create(
            google_id=111,
            user_name="Test",
            user_email="a@b.com",
        )
        user = User.get_by_user_id(user_id)
        with client.application.test_request_context():
            login_user(user)
            with client.session_transaction() as sess:
                sess["_user_id"] = 1


@pytest.fixture
def basic_recipe():
    return {
        "user_id": 1,
        "title": "A Test Menu",
        "description": "Some Description",
        "portion_serves": 1,
        "ingredients": [
            {"ingredient_type": "Salt", "amount": 1.0, "unit": "g"},
            {"ingredient_type": "Pepper", "amount": 1.0, "unit": "g"},
            {"ingredient_type": "Carrot", "amount": 5.0, "unit": "Piece"},
        ],
        "note": "Lorem Ipsum",
        "web_link": "https://google.com",
        "image_url": "https://google.com",
    }


def test_bp_add_recepie_get(client):
    with client.application.app_context():
        user_id = User.create(
            google_id=111,
            user_name="Test",
            user_email="a@b.com",
        )
        user = User.get_by_user_id(user_id)
        with client.application.test_request_context():
            login_user(user)
            with client.session_transaction() as sess:
                sess["_user_id"] = 1
        response = client.get("recipe/add")
        assert response.status_code == 200


def test_bp_add_recepie_post(client, imut_dict_response):
    with client.application.app_context():
        user_id = User.create(
            google_id=111,
            user_name="Test",
            user_email="a@b.com",
        )
        user = User.get_by_user_id(user_id)
        with client.application.test_request_context():
            login_user(user)
            with client.session_transaction() as sess:
                sess["_user_id"] = 1
        response = client.post("recipe/add", data=imut_dict_response)
        assert response.status_code == 200


def test_bp_delete_recipe(client, logged_in_user, basic_recipe):
    with client.application.app_context():
        Recipe.create(**basic_recipe)
        response = client.post("recipe/delete/1", follow_redirects=True)
        assert "deleted" in response.text


def test_bp_delete_recipe_fail(client, logged_in_user, basic_recipe):
    with client.application.app_context():
        Recipe.create(**basic_recipe)
        response = client.post("recipe/delete/2", follow_redirects=True)
        assert "Error deleting recipe with ID:" in response.text


def test_bp_get_recepie_by_user_id(client, logged_in_user, basic_recipe):
    # Logged in user fixture needed for accessing url!!
    with client.application.app_context():
        Recipe.create(**basic_recipe)
        Recipe.create(**basic_recipe)
        Recipe.create(**basic_recipe)
        response = client.get("recipe/list")
        assert "card" in response.text


def test_bp_update_recipe(client, logged_in_user, basic_recipe):
    with client.application.app_context():
        Recipe.create(**basic_recipe)
        data = ImmutableMultiDict(
            [
                ("menuTitle", "Updated Title"),
                ("menuDescription", "Test"),
                ("ingredient_amount[0]", "2"),
                ("ingredient_unit[0]", "g"),
                ("ingredient_type[0]", "Salz"),
                ("ingredient_amount[1]", "1"),
                ("ingredient_unit[1]", "g"),
                ("ingredient_type[1]", "Pfeffer"),
                ("menuServes", "2"),
                ("menuNote", "Test"),
                ("menuPrepLink", "http://www.google.com"),
                ("menuImageUrl", "http://www.google.com"),
            ]
        )
        response = client.post("recipe/update/1", data=data, follow_redirects=True)
        assert response.status_code == 200
        recipe = Recipe.get_recipe_by_id(1)
        assert recipe["title"] == "Updated Title"
        assert recipe["ingredients"][0]["amount"] == 2


def test_bp_edit_endpoint(client, logged_in_user):
    with client.application.app_context():
        response = client.get("recipe/edit/1")
        assert response.status_code == 200


def test_bp_recipe_to_shoppinglist(client, basic_recipe, logged_in_user):
    with client.application.app_context():
        Recipe.create(**basic_recipe)
        response = client.get("recipe/list", follow_redirects=True)
        assert "Edit" in response.text
        assert "Get Shoppinglist" in response.text
