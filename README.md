# Dishcrafter

## Why
Becoming a parent involves a lot. One new duty is ensure that the household provides all the ingredients for the planned meals. Now the
meals of course need variety of healthy and nutritious ingredients to ensure todays requirements toward a healthy
lifestyle. Planning meals, taking note of ingredients and summarizing them seemed something one could automate. 

That is why there is `Dishcrafter`. Keep your recipes in one place, plan them with the meal planner and get the summarized ingredients. The ingredients can be exported to [Bring!](https://www.getbring.com/en/home)

So the general problem `Dishcrafter` tries to solve is creating a meal plan from a bunch of recipes and export them to a shopping list.
![ALT](readme_images/dishcrafter_general_problem.svg)
*The Problem `Dishcrafter` tries to solve. (source: via [excalidraw.com](https://excalidraw.com/))*

## How
Currently `Dishcrafter` uses a Python Flask backend to handle the serving of the app. An SQLite Database to handle the storage of the recipes and a tailwind css styling approach. Javascript is used to handle the frontend behavior.
![ALT](readme_images/dishcrafter_tech_stack.svg)
*`Dishcrafter` tech stack. (source: via [excalidraw.com](https://excalidraw.com/))*

First one needs to add recipes. After the recipes are added a meal plan can be created via drag and drop. Then from each planned recipe, the ingredients are extracted an presented as a shopping list.
![ALT](readme_images/dishcrafter_basic_flow.svg)

# Quickstart Guide Local Deployment
Prerequisites:
- docker installed
- git installed

Clone Repo & Run docker container:
1. `git clone https://gitlab.com/jonnybronco/dishcrafter.git`

2. `cd dishcrafter/`

3. `docker build -t dishcrafter .`

4. Run container in local login mode:
```
docker run -it \
-p 5000:5000 \
-e LOCAL_USER=true \
dishcrafter
```

## SQLite Database

`Dishcrafter` stores all information within the container in a sqlite database file, which is located in `/instance/dishcrafter.sqlite`. One simple way to persist the data over container restarts is using a `docker volume`:

```
docker volume create dishcrafter_sqlite
docker run -it \
-p 5000:5000 \
-e LOCAL_USER=true \
-v dishcrafter_sqlite:/instance \
dishcrafter
```
## Google OAuth

Currently `Dishcrafter` features google OAuth. In order for this to work, please follow the docs at google [(Scenarios - Web server)](https://developers.google.com/identity/protocols/oauth2#scenarios).

What one should end up with is a two variables:
````
GOOGLE_CLIENT_ID
GOOGLE_CLIENT_SECRET
````
now, one can run the container using these env vars instead of the local user:

```
docker volume create dishcrafter_sqlite
docker run -it \
-p 5000:5000 \
-e GOOGLE_CLIENT_ID=<client-id>
-e GOOGLE_CLIENT_SECRET=<secret>
-v dishcrafter_sqlite:/instance \
dishcrafter
```

Ensure that the redirect URIs match your setup, as described [here](https://developers.google.com/identity/protocols/oauth2/web-server#creatingcred), for local testing you could use 
`https://localhost:5000` or also `https://127.0.0.1:5000`. 

> Attention: Ensure `https` instead of `http`

# Development

## VS Code launch.json

The following launch config helps one to run the `Dishcrafter` locally with VS Code. Ensure that you
have setup a valid python env, and that the python packages are installed from `requirements.txt`.
````
{
  "configurations": [
    {
        "name": "Python: DishCrafter",
        "type": "debugpy",
        "request": "launch",
        "program": "${workspaceFolder}/app/main.py",
        "justMyCode": true,
        "console": "integratedTerminal",
        "env": {
            "FLASK_ENV" : "development",
            "LOCAL_USER" : "true",
            "PYTHONPATH": "${workspaceFolder}:${workspaceFolder}/app"
        }
    }
  ]
}
````

## Pytest

The following excerpt of `settings.json` enables the tests in VS Code with `pytest`:
````
{
    "python.testing.pytestArgs": [
        "tests"
    ],
    "python.testing.unittestEnabled": false,
    "python.testing.pytestEnabled": true
}
````

## Jest

For `Jest` to run `npm` needs to be installed, and also `npm install` needs to be run once. Then one can run `npm run test` and the Jest Testing Suite runs.

> **TODO**: The coverage of the JS files is currently quiet low --> Either re-write or ensure good coverage.

## Tailwind CSS

In order to be able to see the changes made in html classes, which use tailwind, the tailwind build process has
to be running in the background as described here: https://tailwindcss.com/docs/installation
For this `cd` into `dishcrafter/app` and run `npx tailwindcss -i static/input.css -o static/output.css --watch --verbose`.

## Linting

For Python, black is used and for JS and HTML prettier is used. In order to pass the CI tests, run `black app` and `npx prettier --write "**/*.{js,html,css}"`.